Tinytown
========

What's this?
------------

A game engine for a brick building game. At the moment all it can really do is render Blockland
save files, which I eventually plan to remove from the engine. It's just to test the engine.

While I say that it can render Blockland save files, I don't mean it can render everything, all it
can render are bricks in the colors of the specified palette and only a subset of non-special
bricks. It cannot render bricks such as trees. It tries to guess brick geometry from the brick
name, it cannot actually load brick definitions. Maybe it never will, it's out of scope for the
project.

Also, you may need to convert the save file to valid UTF-8, as this monstrous save file format
doesn't even use the One True Standard for text encoding.

How do I use it?
----------------

Well, if you want to use it, you can use it like so:

```
cargo run --release -- path/to/save_file.bls
```

You'll first need to get some textures from a Blockland installation, though, since I cannot
redistribute these. The textures I'm talking about are these:

  - `base/data/shapes/brickTOP.png`
  - `base/data/shapes/brickSIDE.png`
  - `base/data/shapes/brickBOTTOMLOOP.png`

What's planned for the future?
------------------------------

No idea, maybe I will actually make something cool, but most likely I'll get disinterested and
abandon this.

If I do continue:

  - More face culling, since there are a lot of optimizations possible there
  - Loading .obj models
  - Special bricks
  - Better collision detection:
    - Collision between two non-brick entities
    - Time-of-impact, so you can't run through walls when you're fast enough
  - Maybe even building bricks in-game
  - UI

What license is this under?
---------------------------

MIT license.
