in vec4 v_color;
in vec3 v_normal;
in vec2 v_size;
in vec2 v_uv;

out vec4 frag;

uniform sampler2D tex_top;
uniform sampler2D tex_side;
uniform sampler2D tex_bottom;
uniform vec4 default_color;
uniform vec3 light;

void main() {
  float brightness = 0.2 + ((1. - dot(light, v_normal)) / 2.) * 0.8;
  vec4 sample;
  vec2 tex_uv = mod(v_uv, vec2(1., 1.));
  if(v_normal == vec3(0, 1, 0)) {
    sample = texture(tex_top, tex_uv);
  }
  else if(v_normal == vec3(0, -1, 0)) {
    sample = texture(tex_bottom, tex_uv);
  }
  else {
    vec2 tex_coord = vec2(0.5, 0.5);
    bool modified = false;
    if(v_uv.x < 0.5) {
      tex_coord.x = v_uv.x;
      modified = true;
    }
    if(v_uv.y < 0.5) {
      tex_coord.y = v_uv.y;
      modified = true;
    }
    if(v_size.x - v_uv.x < 0.5) {
      tex_coord.x = 1. + (v_uv.x - v_size.x);
      modified = true;
    }
    if(v_size.y - v_uv.y < 0.5) {
      tex_coord.y = 1 + (v_uv.y - v_size.y);
      modified = true;
    }
    if(modified) {
      sample = texture(tex_side, tex_coord);
    }
    else {
      sample = texture(tex_side, vec2(0.5, 0.5));
    }
  }
  vec3 color = mix(default_color.xyz, v_color.xyz, v_color.w) * sample.xyz;
  frag = vec4(brightness * color, default_color.w);
}
