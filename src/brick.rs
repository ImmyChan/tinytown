use {
    specs::Entity,
    cgmath::{
        prelude::*,
        Vector3,
        Vector2,
        Point3,
        Point2,
        Quaternion,
        Rad,
    },
};

mod brick_data_store;
mod palette;

pub use self::brick_data_store::BrickDataStore;
pub use self::palette::{BrickColorId, BrickPalette};

use crate::{
    mesh::Mesh,
    util::{Directional, Color},
};

pub const VOXEL_SIZE: Vector3<f32> = Vector3 {
    x: 0.5,
    y: 0.2,
    z: 0.5,
};

#[derive(Clone, Copy, Debug, PartialEq, Eq, PartialOrd, Ord, Hash)]
pub struct BrickDataId(pub usize);

#[derive(Clone, Debug)]
pub struct BrickData {
    pub name: String,
    pub size: Vector3<u32>,
}

impl BrickData {
    pub fn radius(&self) -> Vector3<f32> {
        Vector3::new(
            self.size.x as f32 * VOXEL_SIZE.x * 0.5,
            self.size.y as f32 * VOXEL_SIZE.y * 0.5,
            self.size.z as f32 * VOXEL_SIZE.z * 0.5,
        )
    }

    pub fn create_mesh<V: Clone>(&self, covered: Directional<bool>, make_vertex: impl Fn(Point3<f32>, Color, Vector3<f32>, Vector2<f32>, Point2<f32>) -> V) -> Mesh<V> {
        let mut vertices = Vec::new();
        let mut indices = Vec::new();
        let (bw, bh, bd) = (self.size.x, self.size.y, self.size.z);
        let (w, h, d) = (
            bw as f32 * VOXEL_SIZE.x,
            bh as f32 * VOXEL_SIZE.y,
            bd as f32 * VOXEL_SIZE.z,
        );
        let (hw, hh, hd) = (w / 2., h / 2., d / 2.);
        let (bwf, bhf, bdf) = (bw as f32, bh as f32, bd as f32);
        let (x, y, z) = (0., 0., 0.);
        let color = Color::rgba(0., 0., 0., 0.);
        if !covered.up {
            let si = vertices.len() as u32;
            let normal = Vector3::new(0., 1., 0.);
            let tex_size = Vector2::new(bwf, bdf);
            vertices.extend_from_slice(&[
                make_vertex(Point3::new(x - hw, y + hh, z - hd), color, normal, tex_size, Point2::new(0. , 0. )),
                make_vertex(Point3::new(x + hw, y + hh, z - hd), color, normal, tex_size, Point2::new(bwf, 0. )),
                make_vertex(Point3::new(x + hw, y + hh, z + hd), color, normal, tex_size, Point2::new(bwf, bdf)),
                make_vertex(Point3::new(x - hw, y + hh, z + hd), color, normal, tex_size, Point2::new(0. , bdf)),
            ]);
            indices.extend(&[si + 0, si + 1, si + 2, si + 2, si + 3, si + 0]);
        }
        if !covered.down {
            let si = vertices.len() as u32;
            let normal = Vector3::new(0., -1., 0.);
            let tex_size = Vector2::new(bwf, bdf);
            vertices.extend_from_slice(&[
                make_vertex(Point3::new(x - hw, y - hh, z - hd), color, normal, tex_size, Point2::new(0. , 0. )),
                make_vertex(Point3::new(x - hw, y - hh, z + hd), color, normal, tex_size, Point2::new(bwf, 0. )),
                make_vertex(Point3::new(x + hw, y - hh, z + hd), color, normal, tex_size, Point2::new(bwf, bdf)),
                make_vertex(Point3::new(x + hw, y - hh, z - hd), color, normal, tex_size, Point2::new(0. , bdf)),
            ]);
            indices.extend(&[si + 0, si + 1, si + 2, si + 2, si + 3, si + 0]);
        }
        if !covered.north {
            let si = vertices.len() as u32;
            let normal = Vector3::new(0., 0., -1.);
            let tex_size = Vector2::new(bwf, bhf);
            vertices.extend_from_slice(&[
                make_vertex(Point3::new(x - hw, y - hh, z - hd), color, normal, tex_size, Point2::new(0. , 0. )),
                make_vertex(Point3::new(x + hw, y - hh, z - hd), color, normal, tex_size, Point2::new(bwf, 0. )),
                make_vertex(Point3::new(x + hw, y + hh, z - hd), color, normal, tex_size, Point2::new(bwf, bhf)),
                make_vertex(Point3::new(x - hw, y + hh, z - hd), color, normal, tex_size, Point2::new(0. , bhf)),
            ]);
            indices.extend(&[si + 0, si + 1, si + 2, si + 2, si + 3, si + 0]);
        }
        if !covered.south {
            let si = vertices.len() as u32;
            let normal = Vector3::new(0., 0., 1.);
            let tex_size = Vector2::new(bwf, bhf);
            vertices.extend_from_slice(&[
                make_vertex(Point3::new(x - hw, y - hh, z + hd), color, normal, tex_size, Point2::new(0. , 0. )),
                make_vertex(Point3::new(x - hw, y + hh, z + hd), color, normal, tex_size, Point2::new(0. , bhf)),
                make_vertex(Point3::new(x + hw, y + hh, z + hd), color, normal, tex_size, Point2::new(bwf, bhf)),
                make_vertex(Point3::new(x + hw, y - hh, z + hd), color, normal, tex_size, Point2::new(bwf, 0. )),
            ]);
            indices.extend(&[si + 0, si + 1, si + 2, si + 2, si + 3, si + 0]);
        }
        if !covered.west {
            let si = vertices.len() as u32;
            let normal = Vector3::new(-1., 0., 0.);
            let tex_size = Vector2::new(bhf, bdf);
            vertices.extend_from_slice(&[
                make_vertex(Point3::new(x - hw, y - hh, z - hd), color, normal, tex_size, Point2::new(0. , 0. )),
                make_vertex(Point3::new(x - hw, y + hh, z - hd), color, normal, tex_size, Point2::new(bhf, 0. )),
                make_vertex(Point3::new(x - hw, y + hh, z + hd), color, normal, tex_size, Point2::new(bhf, bdf)),
                make_vertex(Point3::new(x - hw, y - hh, z + hd), color, normal, tex_size, Point2::new(0. , bdf)),
            ]);
            indices.extend(&[si + 0, si + 1, si + 2, si + 2, si + 3, si + 0]);
        }
        if !covered.east {
            let si = vertices.len() as u32;
            let normal = Vector3::new(1., 0., 0.);
            let tex_size = Vector2::new(bhf, bdf);
            vertices.extend_from_slice(&[
                make_vertex(Point3::new(x + hw, y - hh, z - hd), color, normal, tex_size, Point2::new(0. , 0. )),
                make_vertex(Point3::new(x + hw, y - hh, z + hd), color, normal, tex_size, Point2::new(0. , bdf)),
                make_vertex(Point3::new(x + hw, y + hh, z + hd), color, normal, tex_size, Point2::new(bhf, bdf)),
                make_vertex(Point3::new(x + hw, y + hh, z - hd), color, normal, tex_size, Point2::new(bhf, 0. )),
            ]);
            indices.extend(&[si + 0, si + 1, si + 2, si + 2, si + 3, si + 0]);
        }
        Mesh::new(vertices, indices)
    }
}

#[derive(Clone, Copy, Debug, PartialEq, Eq, PartialOrd, Ord, Hash)]
pub struct BrickOrientation(pub u8);

impl BrickOrientation {
    pub fn next(self) -> BrickOrientation {
        BrickOrientation((self.0 + 1) % 4)
    }

    pub fn prev(self) -> BrickOrientation {
        if self.0 == 0 {
            BrickOrientation(3)
        }
        else {
            BrickOrientation(self.0 - 1)
        }
    }

    pub fn to_quaternion(self) -> Quaternion<f32> {
        let angle = Rad::turn_div_4() * self.0 as f32;
        Quaternion::from_angle_y(angle)
    }
}

#[derive(Clone, Debug)]
pub enum BrickEvent {
    Build {
        data_id: BrickDataId,
        color_id: BrickColorId,
        orientation: BrickOrientation,
        pos: Point3<i32>,
    },
    Destroy {
        entity: Entity,
    },
}

#[derive(Clone, Debug)]
pub struct BrickEventQueue {
    data: Vec<BrickEvent>,
    pub dirty: bool,
}

impl BrickEventQueue {
    pub fn new() -> BrickEventQueue {
        BrickEventQueue {
            data: Vec::new(),
            dirty: true,
        }
    }

    pub fn build(&mut self, data_id: BrickDataId, color_id: BrickColorId, orientation: BrickOrientation, pos: Point3<i32>) {
        self.data.push(BrickEvent::Build { data_id, color_id, orientation, pos });
    }

    pub fn destroy(&mut self, entity: Entity) {
        self.data.push(BrickEvent::Destroy { entity });
    }

    pub fn flush(&mut self, mut f: impl FnMut(BrickEvent)) {
        if !self.data.is_empty() {
            self.dirty = true;
        }
        for evt in self.data.drain(..) {
            f(evt);
        }
    }
}
