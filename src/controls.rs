use {
    std::collections::{HashMap, HashSet},
    cgmath::Vector2,
    shrev::EventChannel,
};

#[derive(Clone, Copy, Debug, PartialEq, PartialOrd, Eq, Ord, Hash)]
pub enum Action {
    Press,
    Release,
}

#[derive(Clone, Copy, Debug, PartialEq, PartialOrd, Eq, Ord, Hash)]
pub enum MouseButton {
    Left,
    Middle,
    Right,
}

#[derive(Debug, Clone, Copy, PartialEq, PartialOrd, Eq, Ord, Hash)]
pub enum Key {
    Escape,
    Space,
    A,
    B,
    C,
    D,
    E,
    F,
    G,
    H,
    I,
    J,
    K,
    L,
    M,
    N,
    O,
    P,
    Q,
    R,
    S,
    T,
    U,
    V,
    W,
    X,
    Y,
    Z,
    LeftShift,
    RightShift,
}

#[derive(Clone, Copy, Debug, PartialEq, PartialOrd, Eq, Ord, Hash)]
pub enum Control {
    Exit,
    MoveForward,
    MoveBackward,
    MoveLeft,
    MoveRight,
    MoveGhostForward,
    MoveGhostBackward,
    MoveGhostLeft,
    MoveGhostRight,
    MoveGhostUp,
    MoveGhostDown,
    MoveGhostProportional,
    RotateGhostCW,
    RotateGhostCCW,
    CycleGhostPrev,
    CycleGhostNext,
    PalettePrev,
    PaletteNext,
    DispelGhost,
    Jump,
    Crawl,
    Build,
    Interact,
    Jetpack,
}

#[derive(Clone, Debug)]
pub struct ControlConfig {
    pub keyboard: HashMap<Key, Control>,
    pub mouse: HashMap<MouseButton, Control>,
}

impl Default for ControlConfig {
    fn default() -> ControlConfig {
        let mut keyboard = HashMap::new();
        keyboard.insert(Key::Escape, Control::Exit);
        keyboard.insert(Key::W, Control::MoveForward);
        keyboard.insert(Key::A, Control::MoveLeft);
        keyboard.insert(Key::S, Control::MoveBackward);
        keyboard.insert(Key::D, Control::MoveRight);
        keyboard.insert(Key::LeftShift, Control::Crawl);
        keyboard.insert(Key::RightShift, Control::MoveGhostProportional);
        keyboard.insert(Key::Space, Control::Jump);
        keyboard.insert(Key::B, Control::Build);
        keyboard.insert(Key::H, Control::MoveGhostLeft);
        keyboard.insert(Key::J, Control::MoveGhostBackward);
        keyboard.insert(Key::K, Control::MoveGhostForward);
        keyboard.insert(Key::L, Control::MoveGhostRight);
        keyboard.insert(Key::U, Control::MoveGhostUp);
        keyboard.insert(Key::I, Control::MoveGhostDown);
        keyboard.insert(Key::Y, Control::PalettePrev);
        keyboard.insert(Key::O, Control::PaletteNext);
        keyboard.insert(Key::G, Control::CycleGhostPrev);
        keyboard.insert(Key::T, Control::CycleGhostNext);
        keyboard.insert(Key::P, Control::DispelGhost);
        keyboard.insert(Key::N, Control::RotateGhostCCW);
        keyboard.insert(Key::M, Control::RotateGhostCW);
        let mut mouse = HashMap::new();
        mouse.insert(MouseButton::Left, Control::Interact);
        mouse.insert(MouseButton::Right, Control::Jetpack);
        ControlConfig { keyboard, mouse }
    }
}

#[derive(Debug)]
pub struct Controls {
    pressed: HashSet<Control>,
    mouse_pressed: HashSet<MouseButton>,
    config: ControlConfig,
    cursor_entered: bool,
    cursor_pos: Option<Vector2<f32>>,
    last_cursor_pos: Option<Vector2<f32>>,
    exit_requested: bool,
    pub events: EventChannel<(Action, Control)>,
}

impl Controls {
    pub fn new() -> Controls {
        Controls {
            pressed: HashSet::new(),
            mouse_pressed: HashSet::new(),
            config: ControlConfig::default(),
            cursor_entered: false,
            cursor_pos: None,
            last_cursor_pos: None,
            exit_requested: false,
            events: EventChannel::new(),
        }
    }

    pub fn handle_key_event(&mut self, action: Action, key: Key) {
        if let Some(control) = self.config.keyboard.get(&key) {
            self.events.single_write((action, *control));
            match action {
                Action::Press => {
                    log::info!("control {:?} activated", control);
                    self.pressed.insert(*control);
                }
                Action::Release => {
                    log::info!("control {:?} deactivated", control);
                    self.pressed.remove(control);
                }
            }
        }
    }

    pub fn handle_mouse_event(&mut self, action: Action, button: MouseButton) {
        match action {
            Action::Press => {
                self.mouse_pressed.insert(button);
            }
            Action::Release => {
                self.mouse_pressed.remove(&button);
            }
        }
        if let Some(control) = self.config.mouse.get(&button) {
            self.events.single_write((action, *control));
            match action {
                Action::Press => {
                    log::info!("control {:?} activated", control);
                    self.pressed.insert(*control);
                },
                Action::Release => {
                    log::info!("control {:?} deactivated", control);
                    self.pressed.remove(control);
                },
            }
        }
    }

    pub fn reset(&mut self) {
        self.last_cursor_pos = self.cursor_pos;
    }

    pub fn handle_cursor_pos(&mut self, pos: Vector2<f32>) {
        self.cursor_pos = Some(pos);
    }

    pub fn handle_cursor_enter(&mut self, entered: bool) {
        self.cursor_entered = entered;
        if !entered {
            self.cursor_pos = None;
        }
    }

    pub fn activated(&self, control: Control) -> bool {
        self.pressed.contains(&control)
    }

    pub fn cursor_pos(&self) -> Option<Vector2<f32>> {
        self.cursor_pos
    }

    pub fn last_cursor_pos(&self) -> Option<Vector2<f32>> {
        self.last_cursor_pos
    }

    pub fn request_exit(&mut self) {
        self.exit_requested = true;
    }

    pub fn exit_requested(&self) -> bool {
        self.exit_requested
    }
}
