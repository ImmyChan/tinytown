use {
    core::{
        ops::Range,
        f32::consts::PI,
    },
    cgmath::{
        prelude::*,
        Vector3,
    },
    proptest::{
        prop_compose,
        proptest,
    },
    approx::{
        assert_ulps_eq,
    },
};

prop_compose! {
    pub fn prop_vec3
        (xr: Range<f32>, yr: Range<f32>, zr: Range<f32>)
        (x in xr, y in yr, z in zr)
        -> Vector3<f32> {
        Vector3::new(x, y, z)
    }
}

prop_compose! {
    pub fn prop_vec3_all(ar: Range<f32>)(v in prop_vec3(ar.clone(), ar.clone(), ar)) -> Vector3<f32> {
        v
    }
}

prop_compose! {
    pub fn prop_vec3_unit()(yaw in 0. .. PI, pitch in 0. .. PI) -> Vector3<f32> {
        Vector3::new(pitch.cos() * yaw.cos(), pitch.sin(), pitch.cos() * yaw.sin())
    }
}

proptest! {
    #[test]
    fn prop_vec3_unit_always_unit(unit in prop_vec3_unit()) {
        assert_ulps_eq!(unit.magnitude2(), 1.);
    }
}
