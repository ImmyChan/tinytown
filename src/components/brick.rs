use {
    cgmath::{
        Point3,
        Quaternion,
    },
    specs::prelude::*,
};

use crate::{
    brick::{BrickColorId, BrickDataId, BrickOrientation},
    util::Directional,
};

#[derive(Clone, Debug)]
pub struct Brick {
    pub color_id: BrickColorId,
    pub data_id: BrickDataId,
    pub orientation: BrickOrientation,
    pub pos: Point3<i32>,
    pub covered: Directional<bool>,
}

impl Component for Brick {
    type Storage = FlaggedStorage<Brick, VecStorage<Brick>>;
}

#[derive(Clone, Debug)]
pub struct GhostBrick {
    pub color_id: BrickColorId,
    pub data_id: BrickDataId,
    pub orientation: BrickOrientation,
    pub pos: Point3<i32>,
    pub visual_pos: Point3<f32>,
    pub visual_rot: Quaternion<f32>,
}

impl Component for GhostBrick {
    type Storage = VecStorage<Self>;
}
