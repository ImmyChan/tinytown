use {
    cgmath::{Point3, Quaternion, Vector3},
    specs::prelude::*,
};

use crate::{
    collision::{Shape, TransformedShape},
    util::WorldAABB,
};

#[derive(Debug)]
pub struct Physical {
    pub pos: Point3<f32>,
    pub rot: Quaternion<f32>,
    pub vel: Vector3<f32>,
    pub shape: Shape,
}

impl Physical {
    pub fn new(pos: Point3<f32>, rot: impl Into<Quaternion<f32>>, shape: Shape) -> Physical {
        Physical {
            pos,
            rot: rot.into(),
            vel: Vector3::new(0., 0., 0.),
            shape,
        }
    }

    pub fn world_aabb(&self) -> WorldAABB {
        match self.shape {
            Shape::Rectangle(radius) => {
                // TODO: This is actually incorrect. It doesn't take the rotation into account.
                WorldAABB::new(self.pos, radius)
            }
            Shape::Sphere(radius) => WorldAABB::new(self.pos, Vector3::new(radius, radius, radius)),
        }
    }

    pub fn transformed_shape(&self) -> TransformedShape {
        self.shape.with_transform(self.pos, self.rot)
    }
}

impl Component for Physical {
    type Storage = DenseVecStorage<Physical>;
}
