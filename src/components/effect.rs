use {cgmath::Point3, specs::prelude::*};

use crate::effect::EffectId;

pub struct Effect {
    pub effect_id: EffectId,
    pub source: Point3<f32>,
    pub seed: [u8; 16],
    pub time: f32,
}

impl Component for Effect {
    type Storage = VecStorage<Self>;
}
