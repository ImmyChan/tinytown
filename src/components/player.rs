use specs::prelude::*;

use cgmath::Vector3;

#[derive(Debug)]
pub struct Player {
    pub facing: Vector3<f32>,
    pub can_jump: bool,
    pub ghost_brick: Option<Entity>,
}

impl Player {
    pub fn new(facing: Vector3<f32>) -> Player {
        Player {
            facing,
            can_jump: false,
            ghost_brick: None,
        }
    }
}

impl Component for Player {
    type Storage = HashMapStorage<Player>;
}
