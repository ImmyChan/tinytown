#[derive(Clone, Debug)]
pub struct Directional<T> {
    pub up: T,   // +y
    pub down: T, // -y

    pub north: T, // +z
    pub south: T, // -z

    pub west: T, // -x
    pub east: T, // +x
}

impl<T: Clone> Directional<T> {
    pub fn all(value: T) -> Directional<T> {
        Directional {
            up: value.clone(),
            down: value.clone(),
            north: value.clone(),
            south: value.clone(),
            west: value.clone(),
            east: value.clone(),
        }
    }
}
