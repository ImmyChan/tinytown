use {
    cgmath::{
        Vector3,
        Point3,
    },
};

#[derive(Debug, Clone, Copy, PartialEq)]
pub struct Ray {
    pub origin: Point3<f32>,
    pub dir: Vector3<f32>,
}

impl Ray {
    pub fn new(origin: Point3<f32>, dir: Vector3<f32>) -> Ray {
        Ray {
            origin,
            dir,
        }
    }

    pub fn point_at(self, t: f32) -> Point3<f32> {
        self.origin + t * self.dir
    }

    pub fn intersects_floor(self) -> Option<f32> {
        if self.dir.y >= 0. {
            None
        }
        else {
            Some(self.origin.y / -self.dir.y)
        }
    }
}
