use {
    cgmath::{Point3, Vector3},
    std::mem,
};

use crate::{
    brick::{BrickOrientation, VOXEL_SIZE},
    util::{self, Ray},
};

#[derive(Clone, Copy, Debug, PartialEq)]
pub struct WorldAABB {
    pub center: Point3<f32>,
    pub radius: Vector3<f32>,
}

impl WorldAABB {
    pub fn new(center: Point3<f32>, radius: Vector3<f32>) -> WorldAABB {
        WorldAABB { center, radius }
    }

    pub fn bsw_pos(self) -> Point3<f32> {
        self.center - self.radius
    }

    pub fn une_pos(self) -> Point3<f32> {
        self.center + self.radius
    }

    pub fn intersects(self, other: WorldAABB) -> bool {
        (self.center.x - other.center.x).abs() <= self.radius.x + other.radius.x
            && (self.center.y - other.center.y).abs() <= self.radius.y + other.radius.y
            && (self.center.z - other.center.z).abs() <= self.radius.z + other.radius.z
    }

    pub fn expand_out(self, vel: Vector3<f32>) -> WorldAABB {
        let hvel = vel * 0.5;
        let center = self.center + hvel;
        let radius = self.radius + hvel;
        WorldAABB { center, radius }
    }

    pub fn normal_at(self, pos: Point3<f32>) -> Vector3<f32> {
        let dir = pos - self.center;
        let dir_abs = dir.map(|x| x.abs());
        if dir_abs.x > dir_abs.y {
            if dir_abs.x > dir_abs.z {
                Vector3::new(dir.x.signum(), 0., 0.)
            }
            else {
                Vector3::new(0., 0., dir.z.signum())
            }
        }
        else if dir_abs.y > dir_abs.z {
            Vector3::new(0., dir.y.signum(), 0.)
        }
        else {
            Vector3::new(0., 0., dir.z.signum())
        }
    }

    pub fn contains(self, point: Point3<f32>) -> bool {
        let delta = self.center - point;
        delta.x.abs() < self.radius.x && delta.y.abs() < self.radius.y && delta.z.abs() < self.radius.z
    }

    pub fn cast_ray(self, ray: Ray) -> Option<f32> {
        if self.contains(ray.origin) {
            Some(0.)
        }
        else {
            let mut t = [0.; 3];
            let dir: [f32; 3] = *ray.dir.as_ref();
            let start: [f32; 3] = *ray.origin.as_ref();
            let min: [f32; 3] = *(self.center - self.radius).as_ref();
            let max: [f32; 3] = *(self.center + self.radius).as_ref();
            for i in 0..3 {
                if dir[i] > 0. {
                    t[i] = (min[i] - start[i]) / dir[i];
                }
                else {
                    t[i] = (max[i] - start[i]) / dir[i];
                }
            }
            let mi = if t[0] > t[1] {
                if t[0] > t[2] {
                    0
                } else {
                    2
                }
            } else if t[1] > t[2] {
                1
            } else {
                2
            };
            if t[mi] > 0. {
                let pt: [f32; 3] = *ray.point_at(t[mi]).as_ref();
                let i2 = (mi + 1) % 3;
                let i3 = (mi + 2) % 3;
                if pt[i2] >= min[i2] && pt[i2] <= max[i2] && pt[i3] >= min[i3] && pt[i3] <= max[i3] {
                    Some(t[mi])
                }
                else {
                    None
                }
            }
            else {
                None
            }
        }
    }
}

#[derive(Clone, Copy, Debug, PartialEq, Hash)]
pub struct BrickAABB {
    pub unw: Point3<i32>,
    pub size: Vector3<u32>,
}

impl BrickAABB {
    pub fn new(unw: Point3<i32>, size: Vector3<u32>) -> BrickAABB {
        BrickAABB { unw, size }
    }

    pub fn new_orient(
        unw: Point3<i32>,
        mut size: Vector3<u32>,
        BrickOrientation(orient): BrickOrientation,
    ) -> BrickAABB {
        if orient % 2 == 1 {
            mem::swap(&mut size.x, &mut size.z);
        }
        BrickAABB { unw, size }
    }
}

impl From<BrickAABB> for WorldAABB {
    fn from(brick_aabb: BrickAABB) -> WorldAABB {
        let unw = util::brick_to_world(brick_aabb.unw);
        let bsize = brick_aabb.size;
        let radius = Vector3::new(
            bsize.x as f32 * VOXEL_SIZE.x * 0.5,
            bsize.y as f32 * VOXEL_SIZE.y * 0.5,
            bsize.z as f32 * VOXEL_SIZE.z * 0.5,
        );
        WorldAABB {
            center: unw + radius,
            radius,
        }
    }
}
