use cgmath::{prelude::*, Point3, Vector3};

use super::Midpoint;

pub trait Support {
    fn support_point(&self, dir: Vector3<f32>) -> Point3<f32>;
}

#[derive(Copy, Clone)]
pub struct MinkowskiDifference<'a, 'b, A: Support + 'a, B: Support + 'b> {
    pub first: &'a A,
    pub second: &'b B,
}

impl<'a, 'b, A: Support + 'a, B: Support + 'b> MinkowskiDifference<'a, 'b, A, B> {
    pub fn new(first: &'a A, second: &'b B) -> MinkowskiDifference<'a, 'b, A, B> {
        MinkowskiDifference { first, second }
    }
}

impl<'a, 'b, A: Support + Midpoint + 'a, B: Support + Midpoint + 'b> Midpoint
    for MinkowskiDifference<'a, 'b, A, B>
{
    fn midpoint(&self) -> Point3<f32> {
        Point3::from_vec(self.first.midpoint() - self.second.midpoint())
    }
}

impl<'a, 'b, A: Support + 'a, B: Support + 'b> Support for MinkowskiDifference<'a, 'b, A, B> {
    fn support_point(&self, dir: Vector3<f32>) -> Point3<f32> {
        Point3::from_vec(self.first.support_point(dir) - self.second.support_point(-dir))
    }
}
