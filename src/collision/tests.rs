use {
    core::{
        f32::consts::PI,
    },
    cgmath::{
        prelude::*,
        Point3,
        Quaternion,
        Rad,
        Vector3,
    },
    proptest::{
        test_runner::Config,
        proptest,
        prop_compose,
    },
    approx::{
        assert_ulps_eq,
    },
};

use crate::{
    testutil::{
        prop_vec3_all,
        prop_vec3_unit,
    },
    consts::{
        TOLERANCE,
    },
};

use super::{
    gjk_collide, CollisionInfo, Midpoint, MinkowskiDifference, Shape, Support, TransformedShape,
};

fn rectangle(pos: Point3<f32>, radius: Vector3<f32>) -> TransformedShape {
    Shape::Rectangle(radius).with_transform(pos, Quaternion::one())
}

fn sphere(pos: Point3<f32>, radius: f32) -> TransformedShape {
    Shape::Sphere(radius).with_transform(pos, Quaternion::one())
}

const XPLUS: Vector3<f32> = Vector3 {
    x: 1.,
    y: 0.,
    z: 0.,
};
const XMINUS: Vector3<f32> = Vector3 {
    x: -1.,
    y: 0.,
    z: 0.,
};
const YPLUS: Vector3<f32> = Vector3 {
    x: 0.,
    y: 1.,
    z: 0.,
};
const YMINUS: Vector3<f32> = Vector3 {
    x: 0.,
    y: -1.,
    z: 0.,
};
const ZPLUS: Vector3<f32> = Vector3 {
    x: 0.,
    y: 0.,
    z: 1.,
};
const ZMINUS: Vector3<f32> = Vector3 {
    x: 0.,
    y: 0.,
    z: -1.,
};

#[test]
fn shape_support_points_rectangle() {
    let rect1 = Shape::Rectangle(Vector3::new(1., 2., 3.));

    assert_ulps_eq!(rect1.support_point(XPLUS).x, 1.);
    assert_ulps_eq!(rect1.support_point(YPLUS).y, 2.);
    assert_ulps_eq!(rect1.support_point(ZPLUS).z, 3.);
    assert_ulps_eq!(rect1.support_point(XMINUS).x, -1.);
    assert_ulps_eq!(rect1.support_point(YMINUS).y, -2.);
    assert_ulps_eq!(rect1.support_point(ZMINUS).z, -3.);

    assert_ulps_eq!(
        rect1.support_point(Vector3::new(1., -1., 1.)),
        Point3::new(1., -2., 3.)
    );

    let rect1t = rect1.with_transform(Point3::new(2., -3., 4.), Quaternion::one());

    assert_ulps_eq!(rect1t.support_point(XPLUS).x, 3.);
    assert_ulps_eq!(rect1t.support_point(YPLUS).y, -1.);
    assert_ulps_eq!(rect1t.support_point(ZPLUS).z, 7.);
    assert_ulps_eq!(rect1t.support_point(XMINUS).x, 1.);
    assert_ulps_eq!(rect1t.support_point(YMINUS).y, -5.);
    assert_ulps_eq!(rect1t.support_point(ZMINUS).z, 1.);

    assert_ulps_eq!(
        rect1t.support_point(Vector3::new(1., -1., -1.)),
        Point3::new(3., -5., 1.)
    );

    let rect1r = rect1.with_transform(
        Point3::new(0., 0., 0.),
        Quaternion::from_angle_x(Rad(PI * 0.5)),
    );

    assert_ulps_eq!(rect1r.support_point(XPLUS).x, 1.);
    assert_ulps_eq!(rect1r.support_point(YPLUS).y, 3.);
    assert_ulps_eq!(rect1r.support_point(ZPLUS).z, 2.);
    assert_ulps_eq!(rect1r.support_point(XMINUS).x, -1.);
    assert_ulps_eq!(rect1r.support_point(YMINUS).y, -3.);
    assert_ulps_eq!(rect1r.support_point(ZMINUS).z, -2.);
}

#[test]
fn minkowski_difference_rectangle() {
    let rect1 = Shape::Rectangle(Vector3::new(1., 2., 3.));
    let rect2 = Shape::Rectangle(Vector3::new(2., 2., 4.));
    let diff = MinkowskiDifference::new(&rect1, &rect2);

    assert_ulps_eq!(diff.support_point(XPLUS).x, 3.);
    assert_ulps_eq!(diff.support_point(YPLUS).y, 4.);
    assert_ulps_eq!(diff.support_point(ZPLUS).z, 7.);
    assert_ulps_eq!(diff.support_point(XMINUS).x, -3.);
    assert_ulps_eq!(diff.support_point(YMINUS).y, -4.);
    assert_ulps_eq!(diff.support_point(ZMINUS).z, -7.);

    let rect1 = Shape::Rectangle(Vector3::new(2., 1., 1.))
        .with_transform(Point3::new(1., 1., 1.), Quaternion::one());
    let rect2 = Shape::Rectangle(Vector3::new(4., 1., 1.))
        .with_transform(Point3::new(3., 1., 1.), Quaternion::one());
    let diff = MinkowskiDifference::new(&rect1, &rect2);

    assert_ulps_eq!(diff.support_point(XPLUS).x, 4.);
    assert_ulps_eq!(diff.support_point(YPLUS).y, 2.);
    assert_ulps_eq!(diff.support_point(ZPLUS).z, 2.);
    assert_ulps_eq!(diff.support_point(XMINUS).x, -8.);
    assert_ulps_eq!(diff.support_point(YMINUS).y, -2.);
    assert_ulps_eq!(diff.support_point(ZMINUS).z, -2.);

    // TODO: test rotations
}

fn gjk_collide_test<A, B>(shape_a: &A, shape_b: &B) -> Option<CollisionInfo>
where
    A: Support + Midpoint,
    B: Support + Midpoint,
{
    let fwd = gjk_collide(shape_a, shape_b);
    let bwd = gjk_collide(shape_b, shape_a);
    assert_eq!(fwd.is_some(), bwd.is_some());
    fwd
}

#[test]
fn gjk_collision_works() {
    let rect1 = rectangle(Point3::new(1., 0., 0.), Vector3::new(2., 1., 1.));
    let rect2 = rectangle(Point3::new(3., 1., 0.), Vector3::new(4., 2., 2.));
    let rect3 = rectangle(Point3::new(6., 0., 0.), Vector3::new(2., 1., 1.));
    let rect4 = rectangle(Point3::new(0., 0., 0.), Vector3::new(10., 10., 10.));
    let rect5 = rectangle(Point3::new(3., 1., 0.), Vector3::new(4., 1., 2.));

    // Rect1 collides with rect2, rect2 collides with rect3, rect3 does not collide with rect1.
    assert!(gjk_collide_test(&rect1, &rect2).is_some());
    assert!(gjk_collide_test(&rect2, &rect3).is_some());
    assert!(gjk_collide_test(&rect1, &rect3).is_none());

    // Rect4 collides with everything.
    assert!(gjk_collide_test(&rect1, &rect4).is_some());
    assert!(gjk_collide_test(&rect2, &rect4).is_some());
    assert!(gjk_collide_test(&rect3, &rect4).is_some());
    assert!(gjk_collide_test(&rect4, &rect1).is_some());
    assert!(gjk_collide_test(&rect4, &rect2).is_some());
    assert!(gjk_collide_test(&rect4, &rect3).is_some());
    assert!(gjk_collide_test(&rect4, &rect4).is_some());

    // Rect7 collides with rect1, rect2 and rect3.
    assert!(gjk_collide_test(&rect1, &rect5).is_some());
    assert!(gjk_collide_test(&rect2, &rect5).is_some());
    assert!(gjk_collide_test(&rect3, &rect5).is_some());
}

#[test]
fn gjk_collision_works_detailed() {
    fn gjk_collide_test_detailed<A, B>(shape_a: &A, shape_b: &B, normal: Vector3<f32>, depth: f32)
    where
        A: Support + Midpoint,
        B: Support + Midpoint,
    {
        let fwd = gjk_collide(shape_a, shape_b).unwrap();
        let bwd = gjk_collide(shape_b, shape_a).unwrap();
        assert_ulps_eq!(fwd.normal.magnitude(), 1.);
        assert_ulps_eq!(bwd.normal.magnitude(), 1.);
        assert!(fwd.depth >= 0., "{} < 0", fwd.depth);
        assert!(bwd.depth >= 0., "{} < 0", bwd.depth);
        assert_ulps_eq!(fwd.depth, bwd.depth);
        assert_ulps_eq!(fwd.normal, normal);
        assert_ulps_eq!(fwd.depth, depth);
    }

    gjk_collide_test_detailed(
        &rectangle(Point3::new(0., 0., 0.), Vector3::new(1., 1., 1.)),
        &rectangle(Point3::new(1., 0., 0.), Vector3::new(1., 1., 1.)),
        Vector3::new(1., 0., 0.),
        1.,
    );
}

proptest! {
    #![proptest_config(Config::with_cases(3000))]
    #[test]
    fn proptest_rect_rect_collision(
        pos_a in prop_vec3_all(-2. .. 2.),
        pos_b in prop_vec3_all(-2. .. 2.),
        radius_a in prop_vec3_all(0.1 .. 2.),
        radius_b in prop_vec3_all(0.1 .. 2.),
    ) {
        let dx = (pos_b - pos_a).map(|x| x.abs());
        let rsum = radius_a + radius_b;
        let collides = dx.x < rsum.x && dx.y < rsum.y && dx.z < rsum.z;
        let shape_a = rectangle(Point3::from_vec(pos_a), radius_a);
        let shape_b = rectangle(Point3::from_vec(pos_b), radius_b);
        let result_ab = gjk_collide(&shape_a, &shape_b);
        let result_ba = gjk_collide(&shape_b, &shape_a);
        assert_eq!(result_ab.is_some(), collides);
        assert_eq!(result_ba.is_some(), collides);
        if let (Some(rab), Some(rba)) = (result_ab, result_ba) {
            assert_ulps_eq!(rab.depth, rba.depth);
            assert!(rab.depth > 0.);
            assert!(rba.depth > 0.);
            assert_ulps_eq!(rab.normal.magnitude(), 1.);
            assert_ulps_eq!(rba.normal.magnitude(), 1.);
        }
    }
}

proptest! {
    #![proptest_config(Config::with_cases(3000))]
    #[test]
    fn proptest_sphere_sphere_collision(
        pos_a in prop_vec3_all(-2. .. 2.),
        pos_b in prop_vec3_all(-2. .. 2.),
        radius_a in 0.1f32 .. 2.,
        radius_b in 0.1f32 .. 2.,
    ) {
        let dx = (pos_b - pos_a).map(|x| x.abs());
        let rsum = radius_a + radius_b;
        let collides = dx.magnitude() < rsum;
        let shape_a = sphere(Point3::from_vec(pos_a), radius_a);
        let shape_b = sphere(Point3::from_vec(pos_b), radius_b);
        let result_ab = gjk_collide(&shape_a, &shape_b);
        let result_ba = gjk_collide(&shape_b, &shape_a);
        assert_eq!(result_ab.is_some(), collides);
        assert_eq!(result_ba.is_some(), collides);
        if let (Some(rab), Some(rba)) = (result_ab, result_ba) {
            assert_ulps_eq!(rab.depth, rba.depth, epsilon = TOLERANCE);
            assert!(rab.depth > 0.);
            assert!(rba.depth > 0.);
            assert_ulps_eq!(rab.normal.magnitude(), 1.);
            assert_ulps_eq!(rba.normal.magnitude(), 1.);
        }
    }
}

proptest! {
    #![proptest_config(Config::with_cases(3000))]
    #[test]
    fn proptest_sphere_rect_collision_guaranteed(
        sphere_center in prop_vec3_all(-2. .. 2.),
        rect_offset_t in 0.0f32 .. 0.9,
        rect_offset_dir in prop_vec3_unit(),
        sphere_radius in 0.1f32 .. 2.,
        rect_radius in prop_vec3_all(0.1 .. 2.),
    ) {
        let rect_center = sphere_center + rect_offset_dir * (sphere_radius * rect_offset_t);
        let shape_a = rectangle(Point3::from_vec(rect_center), rect_radius);
        let shape_b = sphere(Point3::from_vec(sphere_center), sphere_radius);
        let result_ab = gjk_collide(&shape_a, &shape_b);
        let result_ba = gjk_collide(&shape_b, &shape_a);
        assert!(result_ab.is_some());
        assert!(result_ba.is_some());
        let rab = result_ab.unwrap();
        let rba = result_ba.unwrap();
        assert_ulps_eq!(rab.depth, rba.depth, epsilon = TOLERANCE);
        assert!(rab.depth > 0.);
        assert!(rba.depth > 0.);
        assert_ulps_eq!(rab.normal.magnitude(), 1.);
        assert_ulps_eq!(rba.normal.magnitude(), 1.);
    }
}
