use {
    approx::{
        ulps_eq,
    },
    cgmath::{
        prelude::*,
        Point3,
        Vector3,
    },
    std::fmt,
};

use super::{CollisionInfo, Midpoint, MinkowskiDifference, Support};

use crate::consts::{EPA_MAX_ITERATIONS, GJK_MAX_ITERATIONS, TOLERANCE};

#[derive(Clone)]
pub struct Simplex {
    vertices: [Point3<f32>; 4],
    dim: usize,
}

impl fmt::Debug for Simplex {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        match self.dim {
            0 => write!(f, "Simplex::point({:?})", &self.vertices[0]),
            1 => write!(f, "Simplex::line({:?})", &self.vertices[..2]),
            2 => write!(f, "Simplex::triangle({:?})", &self.vertices[..3]),
            3 => write!(f, "Simplex::tetrahedron({:?})", &self.vertices[..4]),
            _ => unreachable!(),
        }
    }
}

impl Simplex {
    pub fn point(a: Point3<f32>) -> Simplex {
        Simplex {
            vertices: [a; 4],
            dim: 0,
        }
    }

    pub fn push(&mut self, point: Point3<f32>) {
        if self.dim < 3 {
            self.dim += 1;
            self.vertices[self.dim] = point;
        } else {
            unimplemented!()
        }
    }

    pub fn get_new_dir_and_cull(&mut self) -> Option<Vector3<f32>> {
        match self.dim {
            3 => self.tetrahedral_case(),
            2 => Some(self.triangle_case()),
            1 => Some(cross_aba(
                self.vertices[0] - self.vertices[1],
                Point3::origin() - self.vertices[1],
            )),
            0 => Some(Point3::origin() - self.vertices[0]),
            _ => unreachable!(),
        }
    }

    fn tetrahedral_case(&mut self) -> Option<Vector3<f32>> {
        let (a, b, c, d) = (
            self.vertices[3],
            self.vertices[2],
            self.vertices[1],
            self.vertices[0],
        );
        let ab = b - a;
        let ac = c - a;
        let ao = Point3::origin() - a;
        let abc = ab.cross(ac);
        assert!(abc.dot(d - a) <= 0.);
        if abc.dot(ao) > TOLERANCE {
            self.vertices[2] = a;
            self.vertices[1] = b;
            self.vertices[0] = c;
            self.dim = 2;
            Some(self.triangle_case())
        } else {
            let ad = d - a;
            let acd = ac.cross(ad);
            assert!(acd.dot(b - a) <= 0.);
            if acd.dot(ao) > TOLERANCE {
                self.vertices[2] = a;
                self.vertices[1] = c;
                self.vertices[0] = d;
                self.dim = 2;
                Some(self.triangle_case())
            } else {
                let adb = ad.cross(ab);
                assert!(adb.dot(c - a) <= 0.);
                if adb.dot(ao) > TOLERANCE {
                    self.vertices[2] = a;
                    self.vertices[1] = d;
                    self.vertices[0] = b;
                    self.dim = 2;
                    Some(self.triangle_case())
                } else {
                    None
                }
            }
        }
    }

    fn triangle_case(&mut self) -> Vector3<f32> {
        let (a, b, c) = (self.vertices[2], self.vertices[1], self.vertices[0]);
        let ab = b - a;
        let ac = c - a;
        let ao = Point3::origin() - a;
        let abc = ab.cross(ac);
        if abc.cross(ac).dot(ao) > 0. {
            if ac.dot(ao) > 0. {
                self.vertices[0] = c;
                self.vertices[1] = a;
                self.dim = 1;
                cross_aba(ac, ao)
            } else if ab.dot(ao) > 0. {
                self.vertices[0] = b;
                self.vertices[1] = a;
                self.dim = 1;
                cross_aba(ab, ao)
            } else {
                self.vertices[0] = a;
                self.dim = 0;
                ao
            }
        } else if ab.cross(abc).dot(ao) > 0. {
            if ab.dot(ao) > 0. {
                self.vertices[0] = b;
                self.vertices[1] = a;
                self.dim = 2;
                cross_aba(ab, ao)
            } else {
                self.vertices[0] = a;
                self.dim = 0;
                ao
            }
        } else if abc.dot(ao) > 0. {
            abc
        } else {
            self.vertices.swap(0, 1);
            -abc
        }
    }
}

fn cross_aba(a: Vector3<f32>, b: Vector3<f32>) -> Vector3<f32> {
    let ret = a.cross(b).cross(a);
    if ulps_eq!(ret, Vector3::zero()) {
        a.cross(b + Vector3::new(1., 0., 0.)).cross(a)
    } else {
        ret
    }
}

pub fn gjk<A: Support + Midpoint>(shape: &A) -> Option<Simplex> {
    let mut midpoint = shape.midpoint();
    if ulps_eq!(midpoint, Point3::origin()) {
        midpoint.x += 0.00001;
    }
    let mut dir = midpoint - Point3::origin();
    let initial_point = shape.support_point(dir);
    let mut simplex = Simplex::point(initial_point);
    dir = -dir;
    for _ in 0..GJK_MAX_ITERATIONS {
        let support_point = shape.support_point(dir);
        if support_point.dot(dir) <= 0. {
            return None;
        }
        simplex.push(support_point);
        if let Some(new_dir) = simplex.get_new_dir_and_cull() {
            dir = new_dir;
            if ulps_eq!(dir, Vector3::zero()) {
                dir = Vector3::new(0.1, 0.2, 0.3);
            }
        } else {
            return Some(simplex);
        }
    }
    log::error!("GJK max iterations exhausted, giving up");
    None
}

pub fn gjk_collide<A: Support + Midpoint, B: Support + Midpoint>(
    shape_a: &A,
    shape_b: &B,
) -> Option<CollisionInfo> {
    let minkowski_diff = MinkowskiDifference::new(shape_a, shape_b);
    gjk(&minkowski_diff).and_then(|simplex| {
        let penetration = epa(&minkowski_diff, simplex);
        if ulps_eq!(penetration, Vector3::zero()) {
            None
        } else {
            Some(CollisionInfo {
                normal: penetration.normalize(),
                depth: penetration.magnitude(),
            })
        }
    })
}

pub fn gjk_cast_ray<S: Support + Midpoint>(origin: Point3<f32>, dir: Vector3<f32>, shape: &S) -> Option<(Point3<f32>, Vector3<f32>)> {
    unimplemented!()
    // TODO
}

#[derive(Clone, Debug)]
struct Polytope {
    faces: Vec<Face>,
}

impl Polytope {
    pub fn from_simplex(simplex: Simplex) -> Polytope {
        assert_eq!(simplex.dim, 3);
        let (a, b, c, d) = (
            simplex.vertices[3],
            simplex.vertices[2],
            simplex.vertices[1],
            simplex.vertices[0],
        );
        let faces = vec![
            Face::new([a, b, c]),
            Face::new([a, c, d]),
            Face::new([a, d, b]),
            Face::new([c, b, d]),
        ];
        let poly = Polytope { faces };
        #[cfg(test)]
        poly.check_faces_point_outwards();
        poly
    }

    #[cfg(test)]
    pub fn check_faces_point_outwards(&self) {
        let mut inwards = 0;
        for face in &self.faces {
            if face.normal.dot(Point3::origin() - face.vertices[0]) > TOLERANCE {
                inwards += 1;
            }
        }
        if inwards > 0 {
            panic!("{} polytope faces are pointing inwards!", inwards);
        }
    }

    pub fn closest_face(&self) -> &Face {
        let mut best_face = &self.faces[0];
        for face in self.faces[1..].iter() {
            if face.distance < best_face.distance {
                best_face = face;
            }
        }
        best_face
    }

    pub fn add_point(&mut self, point: Point3<f32>) {
        let mut edges: Vec<(Point3<f32>, Point3<f32>)> = Vec::new();
        fn add_edge(
            edges: &mut Vec<(Point3<f32>, Point3<f32>)>,
            start: Point3<f32>,
            end: Point3<f32>,
        ) {
            let len = edges.len();
            edges.retain(|&(a, b)| (a, b) != (end, start));
            if len == edges.len() {
                edges.push((start, end));
            }
        }
        self.faces.retain(|face| {
            let dot = face.normal.dot(point - face.vertices[0]);
            let cull_face = dot > 0.;
            if cull_face {
                let (a, b, c) = (face.vertices[0], face.vertices[1], face.vertices[2]);
                add_edge(&mut edges, a, b);
                add_edge(&mut edges, b, c);
                add_edge(&mut edges, c, a);
            }
            !cull_face
        });
        let new_faces = edges.into_iter().map(|e| Face::new([e.0, e.1, point]));
        self.faces.extend(new_faces);
        #[cfg(test)]
        self.check_faces_point_outwards();
    }
}

#[derive(Clone, Copy, Debug)]
struct Face {
    vertices: [Point3<f32>; 3],
    normal: Vector3<f32>,
    distance: f32,
}

impl Face {
    pub fn new(vertices: [Point3<f32>; 3]) -> Face {
        let (a, b, c) = (vertices[0], vertices[1], vertices[2]);
        #[cfg(test)]
        assert!(
            a != b && b != c,
            "There are overlapping face vertices! {:?}",
            vertices
        );
        let normal = (b - a).cross(c - a).normalize();
        let distance = a.dot(normal);
        #[cfg(test)]
        assert!(
            distance + TOLERANCE >= 0.,
            "face.distance is smaller than zero! ( {} )",
            distance
        );
        Face {
            vertices,
            normal,
            distance,
        }
    }
}

pub fn epa<A: Support>(shape: &A, simplex: Simplex) -> Vector3<f32> {
    let mut poly = Polytope::from_simplex(simplex);
    let mut iterations = 0;
    loop {
        let support_point = {
            let face = poly.closest_face();
            let dir = face.normal;
            let support_point = shape.support_point(dir);
            let dot = support_point.dot(face.normal);
            if dot <= face.distance + TOLERANCE || iterations > EPA_MAX_ITERATIONS {
                return face.normal * face.distance;
            }
            support_point
        };
        poly.add_point(support_point);
        iterations += 1;
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    use super::super::{Shape, TransformedShape};

    use {
        cgmath::{
            Quaternion,
        },
        proptest::{
            test_runner::Config,
            proptest,
        },
    };

    use crate::{
        testutil::prop_vec3_all,
    };

    fn rectangle(pos: Point3<f32>, radius: Vector3<f32>) -> TransformedShape {
        Shape::Rectangle(radius).with_transform(pos, Quaternion::one())
    }

    fn sphere(pos: Point3<f32>, radius: f32) -> TransformedShape {
        Shape::Sphere(radius).with_transform(pos, Quaternion::one())
    }

    #[test]
    fn gjk_works_rect() {
        let shape1 = rectangle(Point3::new(0., 0., 0.), Vector3::new(1., 1., 1.));
        let shape2 = rectangle(Point3::new(5., 7., 3.), Vector3::new(1., 2., 1.));
        let shape3 = rectangle(Point3::new(5., -4., 3.), Vector3::new(1., 2., 1.));
        let shape4 = rectangle(Point3::new(0.9, 0., 0.), Vector3::new(1., 2., 3.));
        let shape5 = rectangle(Point3::new(0.8, 0.8, 0.), Vector3::new(1., 1., 3.));
        let shape6 = rectangle(Point3::new(0.3, 0.4, 0.7), Vector3::new(1., 1., 1.));
        let shape7 = rectangle(Point3::new(0.9, -0.6, 0.9), Vector3::new(1., 1., 1.));
        let shape8 = rectangle(Point3::new(-0.9, -0.8, -0.8), Vector3::new(1., 1., 1.));
        let shape9 = rectangle(Point3::new(-5., 0., 0.), Vector3::new(4., 1., 1.));
        let outppp = rectangle(Point3::new(2., 2., 2.), Vector3::new(1., 1., 1.));
        let outppn = rectangle(Point3::new(2., 2., -2.), Vector3::new(1., 1., 1.));
        let outpnp = rectangle(Point3::new(2., -2., 2.), Vector3::new(1., 1., 1.));
        let outpnn = rectangle(Point3::new(2., -2., -2.), Vector3::new(1., 1., 1.));
        let outnpp = rectangle(Point3::new(-2., 2., 2.), Vector3::new(1., 1., 1.));
        let outnpn = rectangle(Point3::new(-2., 2., -2.), Vector3::new(1., 1., 1.));
        let outnnp = rectangle(Point3::new(-2., -2., 2.), Vector3::new(1., 1., 1.));
        let outnnn = rectangle(Point3::new(-2., -2., -2.), Vector3::new(1., 1., 1.));
        let rect = rectangle(Point3::new(0., 0., 0.), Vector3::new(20., 20., 20.));
        assert!(gjk(&shape1).is_some());
        assert!(gjk(&shape2).is_none());
        assert!(gjk(&shape3).is_none());
        assert!(gjk(&shape1).is_some());
        assert!(gjk(&shape2).is_none());
        assert!(gjk(&shape3).is_none());
        assert!(gjk(&shape4).is_some());
        assert!(gjk(&shape5).is_some());
        assert!(gjk(&shape6).is_some());
        assert!(gjk(&shape7).is_some());
        assert!(gjk(&shape8).is_some());
        assert!(gjk(&shape9).is_none());
        assert!(gjk(&outppp).is_none());
        assert!(gjk(&outppn).is_none());
        assert!(gjk(&outpnp).is_none());
        assert!(gjk(&outpnn).is_none());
        assert!(gjk(&outnpp).is_none());
        assert!(gjk(&outnpn).is_none());
        assert!(gjk(&outnnp).is_none());
        assert!(gjk(&outnnn).is_none());
        assert!(gjk(&rect).is_some());
    }

    #[test]
    fn gjk_works_sphere() {
        let shape1 = sphere(Point3::new(0., 0., 0.), 1.);
        let shape2 = sphere(Point3::new(-0.25585842, -1.7270455, 1.6210499), 3.6433325);
        assert!(gjk(&shape1).is_some());
        assert!(gjk(&shape2).is_some());
    }

    proptest! {
        #![proptest_config(Config::with_cases(3000))]
        #[test]
        fn proptest_spheres_gjk(radius in 0.5f32..5., pos in prop_vec3_all(-2. .. 2.)) {
            let shape = sphere(Point3::from_vec(pos), radius);
            assert_eq!(gjk(&shape).is_some(), pos.magnitude() < radius);
        }
    }

    proptest! {
        #![proptest_config(Config::with_cases(3000))]
        #[test]
        fn proptest_rects_gjk(pos in prop_vec3_all(-2. .. 2.), radius in prop_vec3_all(0.1 .. 2.)) {
            let min = pos - radius;
            let max = pos + radius;
            let contains_origin =
                (min.x < 0. && max.x > 0.) &&
                (min.y < 0. && max.y > 0.) &&
                (min.z < 0. && max.z > 0.);
            let shape = rectangle(Point3::from_vec(pos), radius);
            assert_eq!(gjk(&shape).is_some(), contains_origin);
        }
    }
}
