use cgmath::{prelude::*, Point3, Quaternion, Vector3};

use super::Support;

pub trait Midpoint {
    fn midpoint(&self) -> Point3<f32>;
}

#[derive(Clone, Copy, Debug, PartialEq)]
pub enum Shape {
    Rectangle(Vector3<f32>),
    Sphere(f32),
}

impl Shape {
    pub fn with_transform(self, pos: Point3<f32>, rot: Quaternion<f32>) -> TransformedShape {
        TransformedShape {
            pos,
            rot,
            shape: self,
        }
    }
}

impl Midpoint for Shape {
    #[inline(always)]
    fn midpoint(&self) -> Point3<f32> {
        Point3::origin()
    }
}

impl Support for Shape {
    fn support_point(&self, dir: Vector3<f32>) -> Point3<f32> {
        fn nonzero_signum(x: f32) -> f32 {
            let s = x.signum();
            if s != 0. {
                s
            } else {
                1.
            }
        }
        match *self {
            Shape::Rectangle(radius) => Point3::new(
                radius.x * nonzero_signum(dir.x),
                radius.y * nonzero_signum(dir.y),
                radius.z * nonzero_signum(dir.z),
            ),
            Shape::Sphere(radius) => Point3::origin() + dir.normalize() * radius,
        }
    }
}

#[derive(Clone, Copy, Debug, PartialEq)]
pub struct TransformedShape {
    pub pos: Point3<f32>,
    pub rot: Quaternion<f32>,
    pub shape: Shape,
}

impl Support for TransformedShape {
    fn support_point(&self, dir: Vector3<f32>) -> Point3<f32> {
        self.pos + self.rot * self.shape.support_point(self.rot.invert() * dir).to_vec()
    }
}

impl Midpoint for TransformedShape {
    fn midpoint(&self) -> Point3<f32> {
        self.pos + self.shape.midpoint().to_vec()
    }
}
