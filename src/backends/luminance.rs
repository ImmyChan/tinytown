use {
    cgmath::{
        prelude::*,
        Matrix4,
        Point2,
        Point3,
        Vector2,
        Vector3,
    },
    luminance::{
        context::GraphicsContext,
        face_culling::{FaceCulling, FaceCullingMode, FaceCullingOrder},
        framebuffer::Framebuffer,
        pixel::RGBA32F,
        render_state::RenderState,
        tess::{Mode, Tess, TessBuilder},
        texture::{Dim2, Texture},
        pipeline::PipelineState,
    },
    luminance_glfw::{
        WindowEvent,
        GlfwSurface,
        Surface,
        WindowDim,
        WindowOpt,
        CursorMode,
    },
    specs::{
        prelude::*,
        self,
        Join,
    },
};

use crate::{
    backend::{Backend, BackendName, BrickTextures},
    brick::{BrickDataStore, BrickOrientation, BrickPalette, VOXEL_SIZE, BrickEventQueue},
    camera::Camera,
    components::{Player, Physical, Brick, GhostBrick},
    collision::{CollisionMask, CollisionManager},
    controls::Controls,
    resources::WindowSize,
    texture_manager::{TextureId, TextureManager, TextureManagerProxy},
    util::{self, Ray, Directional},
    mesh::Mesh,
};

mod input;
mod shaders;
mod texture;

use {
    shaders::{
        BrickVertex,
        FloorVertex,
        Shaders,
        VertexPosition,
        VertexColor,
        VertexNormal,
        VertexTexSize,
        VertexTexUv,
    },
    input::{action_convert, key_convert, mouse_button_convert},
    texture::TexLoader,
};

pub struct LuminanceBackend<C: Surface + GraphicsContext = GlfwSurface> {
    ctx: C,
    back_buffer: Framebuffer<Dim2, (), ()>,
    shaders: Shaders,
    light_vector: [f32; 3],
    bricks_tess: Option<Tess>,
    aspect_ratio: f32,
    texture_manager: TextureManager<Texture<Dim2, RGBA32F>>,
    brick_textures: BrickTextures,
    floor_texture: TextureId,
    brick_meshes: Vec<Tess>,
}

impl LuminanceBackend<GlfwSurface> {
    pub fn new(window_size: (u32, u32), title: &str) -> anyhow::Result<LuminanceBackend> {
        let window_opt = WindowOpt::default().set_cursor_mode(CursorMode::Disabled);
        let mut ctx = GlfwSurface::new(
            WindowDim::Windowed(window_size.0, window_size.1),
            title,
            window_opt,
        ).expect("cannot init glfw"); // TODO: throw
        let aspect_ratio = window_size.0 as f32 / window_size.1 as f32;
        let ctx_size = ctx.size();
        let back_buffer = Framebuffer::back_buffer(&mut ctx, ctx_size);
        let shaders = Shaders::new()?;
        let mut texture_manager = TextureManager::<Texture<Dim2, RGBA32F>>::new();
        let brick_textures = BrickTextures::new(&texture_manager.proxy());
        let floor_texture = texture_manager.load("textures/floor.png");
        {
            let mut tex_loader = TexLoader::new(&mut ctx);
            texture_manager.sync(&mut tex_loader)?;
        }
        Ok(LuminanceBackend {
            ctx,
            back_buffer,
            shaders,
            light_vector: Vector3::new(3., -1., 3.).normalize().into(),
            bricks_tess: None,
            aspect_ratio,
            texture_manager,
            brick_textures,
            floor_texture,
            brick_meshes: Vec::new(),
        })
    }
}

impl BackendName for LuminanceBackend<GlfwSurface> {
    fn name() -> &'static str {
        "luminance_glfw"
    }
}

impl Backend for LuminanceBackend<GlfwSurface> {
    fn handle_events(&mut self, world: &specs::World) -> anyhow::Result<()> {
        world.write_resource::<Controls>().reset();
        let mut refresh_framebuffer = None;
        for event in self.ctx.poll_events() {
            match event {
                WindowEvent::Close => {
                    let mut controls = world.write_resource::<Controls>();
                    controls.request_exit();
                },
                WindowEvent::FramebufferSize(width, height) => {
                    refresh_framebuffer = Some((width as u32, height as u32));
                }
                WindowEvent::Key(key, _, action, _) => {
                    if let (Some(key), Some(action)) = (key_convert(key), action_convert(action)) {
                        let mut controls = world.write_resource::<Controls>();
                        controls.handle_key_event(action, key);
                    }
                }
                WindowEvent::MouseButton(button, action, _) => {
                    if let (Some(button), Some(action)) =
                        (mouse_button_convert(button), action_convert(action))
                    {
                        let mut controls = world.write_resource::<Controls>();
                        controls.handle_mouse_event(action, button);
                    }
                }
                WindowEvent::CursorPos(x, y) => {
                    let mut controls = world.write_resource::<Controls>();
                    controls.handle_cursor_pos(Vector2::new(x as f32, y as f32));
                }
                WindowEvent::CursorEnter(entered) => {
                    let mut controls = world.write_resource::<Controls>();
                    controls.handle_cursor_enter(entered);
                }
                _ => (),
            }
        }
        if let Some((width, height)) = refresh_framebuffer {
            let size = [width as u32, height as u32];
            self.back_buffer = Framebuffer::back_buffer(&mut self.ctx, size);
            self.aspect_ratio = size[0] as f32 / size[1] as f32;
            let mut camera = world.write_resource::<Camera>();
            camera.perspective_fov.aspect = self.aspect_ratio;
            let mut ws = world.write_resource::<WindowSize>();
            ws.width = width as u32;
            ws.height = height as u32;
        }
        Ok(())
    }

    fn aspect_ratio(&self) -> f32 {
        self.aspect_ratio
    }

    fn size(&self) -> (u32, u32) {
        (self.back_buffer.width(), self.back_buffer.height())
    }

    fn render(&mut self, world: &specs::World) -> anyhow::Result<()> {
        {
            let mut tex_loader = TexLoader::new(&mut self.ctx);
            self.texture_manager.sync(&mut tex_loader)?;
        }
        let camera = world.read_resource::<Camera>();
        {
            let brick_data_store = world.read_resource::<BrickDataStore>();
            for (i, bd) in brick_data_store.iter() {
                if self.brick_meshes.len() <= i.0 {
                    log::info!("creating mesh for brick id {}, brick name {:?}", i.0, bd.name);
                    let mesh = bd.create_mesh(Directional::all(false), |p, c, n, ts, tc| BrickVertex::create(p, c, n, ts, tc));
                    let tess = self.upload_mesh(&mesh);
                    self.brick_meshes.push(tess);
                }
            }
        }
        {
            let mut beq = world.write_resource::<BrickEventQueue>();
            if beq.dirty {
                self.generate_bricks_tess(world);
                beq.dirty = false;
            }
        }
        let back_buffer = &self.back_buffer;
        let texman = &self.texture_manager;
        let light_vector = self.light_vector;
        let shaders = &self.shaders;
        let bricks_tess = &self.bricks_tess;
        let brick_textures = &self.brick_textures;
        let brick_meshes = &self.brick_meshes;
        let floor_texture = self.floor_texture;
        let floor_tess = {
            let normal = Vector3::new(0., 1., 0.);
            let vertices = [
                    FloorVertex::create(Point3::new(-1000., 0., -1000.), normal, Point2::new(0.   , 0.   )),
                    FloorVertex::create(Point3::new( 1000., 0., -1000.), normal, Point2::new(2000., 0.   )),
                    FloorVertex::create(Point3::new( 1000., 0.,  1000.), normal, Point2::new(2000., 2000.)),
                    FloorVertex::create(Point3::new(-1000., 0.,  1000.), normal, Point2::new(0.   , 2000.)),
            ];
            let indices = [0u32, 1, 2, 2, 3, 0];
            TessBuilder::new(&mut self.ctx)
                .set_mode(Mode::Triangle)
                .add_vertices(&vertices[..])
                .set_indices(&indices[..])
                .build()
                .expect("couldn't build tess")
        };
        {
            let ctx = &mut self.ctx;
            let ps = PipelineState::new()
                .set_clear_color([0., 0., 0., 1.])
                .enable_clear_color(true);
            ctx.pipeline_builder()
                .pipeline(back_buffer, &ps, |pipeline, mut shd_gate| {
                    let top_bound_tex = pipeline.bind_texture(&texman[brick_textures.top]);
                    let bottom_bound_tex = pipeline.bind_texture(&texman[brick_textures.bottom]);
                    let side_bound_tex = pipeline.bind_texture(&texman[brick_textures.side]);
                    let floor_bound_tex = pipeline.bind_texture(&texman[floor_texture]);
                    let render_state = RenderState::default().set_face_culling(FaceCulling::new(
                        FaceCullingOrder::CW,
                        FaceCullingMode::Back,
                    ));
                    shd_gate.shade(&shaders.floor, |iface, mut rdr_gate| {
                        iface.projection.update(camera.projection().into());
                        iface.view.update(camera.view().into());
                        iface.model.update(<Matrix4<_> as One>::one().into()); // TODO: make it follow the camera
                        iface.light.update(light_vector);
                        iface.tex.update(&floor_bound_tex);
                        rdr_gate.render(&render_state, |mut tess_gate| {
                            tess_gate.render(&floor_tess);
                        });
                    });
                    if let Some(ref tess) = *bricks_tess {
                        shd_gate.shade(&shaders.bricks, |iface, mut rdr_gate| {
                            iface.projection.update(camera.projection().into());
                            iface.view.update(camera.view().into());
                            iface.model.update(Matrix4::identity().into());
                            iface.default_color.update([0., 0., 0., 1.]);
                            iface.tex_top.update(&top_bound_tex);
                            iface.tex_bottom.update(&bottom_bound_tex);
                            iface.tex_side.update(&side_bound_tex);
                            iface.light.update(light_vector);
                            rdr_gate.render(&render_state, |mut tess_gate| {
                                tess_gate.render(tess);
                            });
                        });
                    }
                    let bp = world.read_resource::<BrickPalette>();
                    shd_gate.shade(&shaders.bricks, |iface, mut rdr_gate| {
                        iface.projection.update(camera.projection().into());
                        iface.view.update(camera.view().into());
                        iface.tex_top.update(&top_bound_tex);
                        iface.tex_bottom.update(&bottom_bound_tex);
                        iface.tex_side.update(&side_bound_tex);
                        iface.light.update(light_vector);
                        rdr_gate.render(&render_state, |mut tess_gate| {
                            for gb in world.read_storage::<GhostBrick>().join() {
                                iface.default_color.update(bp[gb.color_id].into());
                                let rot: Matrix4<f32> = gb.visual_rot.into();
                                let model = Matrix4::from_translation(gb.visual_pos.to_vec()) * rot;
                                iface.model.update(model.into());
                                let mesh = &brick_meshes[gb.data_id.0];
                                tess_gate.render(mesh);
                            }
                        });
                    });
                });
        }
        self.ctx.swap_buffers();
        Ok(())
    }

    fn texture_manager_proxy(&self) -> TextureManagerProxy {
        self.texture_manager.proxy()
    }
}

impl<C: GraphicsContext + Surface> LuminanceBackend<C> {
    fn upload_mesh<V: luminance::vertex::Vertex>(&mut self, mesh: &Mesh<V>) -> Tess {
        TessBuilder::new(&mut self.ctx)
            .set_mode(Mode::Triangle)
            .add_vertices(&mesh.vertices[..])
            .set_indices(&mesh.indices[..])
            .build()
            .expect("couldn't build tess")
    }

    fn generate_bricks_tess(&mut self, world: &specs::World) {
        fn make_vertex(pos: [f32; 3], col: [f32; 4], normal: [f32; 3], tex_size: [f32; 2], tex_uv: [f32; 2]) -> BrickVertex {
            BrickVertex::new(VertexPosition::new(pos), VertexColor::new(col), VertexNormal::new(normal), VertexTexSize::new(tex_size), VertexTexUv::new(tex_uv))
        }
        let ctx = &mut self.ctx;
        let mut vertices = Vec::new();
        let mut indices: Vec<u32> = Vec::new();
        let palette = world.read_resource::<BrickPalette>();
        let brick_data_store = world.read_resource::<BrickDataStore>();
        for brick in world.read_storage::<Brick>().join() {
            let color: [f32; 4] = palette[brick.color_id].into();
            let data = &brick_data_store[brick.data_id];
            let bsw = util::brick_to_world(brick.pos);
            let BrickOrientation(orient) = brick.orientation;
            let (bx, by, bz) = (bsw.x, bsw.y, bsw.z);
            let size = data.size;
            let (mut bw, bh, mut bd) = (size.x, size.y, size.z);
            if orient % 2 == 1 {
                let t = bw;
                bw = bd;
                bd = t;
            }
            let (w, h, d) = (
                bw as f32 * VOXEL_SIZE.x,
                bh as f32 * VOXEL_SIZE.y,
                bd as f32 * VOXEL_SIZE.z,
            );
            let (hw, hh, hd) = (w / 2., h / 2., d / 2.);
            let (bwf, bhf, bdf) = (bw as f32, bh as f32, bd as f32);
            let (x, y, z) = (bx + hw, by + hh, bz + hd);
            let c = &brick.covered;
            if !c.up {
                let si = vertices.len() as u32;
                let normal = [0., 1., 0.];
                vertices.push(make_vertex(
                    [x - hw, y + hh, z - hd],
                    color,
                    normal,
                    [bwf, bdf],
                    [0., 0.],
                ));
                vertices.push(make_vertex(
                    [x + hw, y + hh, z - hd],
                    color,
                    normal,
                    [bwf, bdf],
                    [bwf, 0.],
                ));
                vertices.push(make_vertex(
                    [x + hw, y + hh, z + hd],
                    color,
                    normal,
                    [bwf, bdf],
                    [bwf, bdf],
                ));
                vertices.push(make_vertex(
                    [x - hw, y + hh, z + hd],
                    color,
                    normal,
                    [bwf, bdf],
                    [0., bdf],
                ));
                indices.extend(&[si + 0, si + 1, si + 2, si + 2, si + 3, si + 0]);
            }
            if !c.down {
                let si = vertices.len() as u32;
                let normal = [0., -1., 0.];
                vertices.push(make_vertex(
                    [x - hw, y - hh, z - hd],
                    color,
                    normal,
                    [bwf, bdf],
                    [0., 0.],
                ));
                vertices.push(make_vertex(
                    [x - hw, y - hh, z + hd],
                    color,
                    normal,
                    [bwf, bdf],
                    [0., bdf],
                ));
                vertices.push(make_vertex(
                    [x + hw, y - hh, z + hd],
                    color,
                    normal,
                    [bwf, bdf],
                    [bwf, bdf],
                ));
                vertices.push(make_vertex(
                    [x + hw, y - hh, z - hd],
                    color,
                    normal,
                    [bwf, bdf],
                    [bwf, 0.],
                ));
                indices.extend(&[si + 0, si + 1, si + 2, si + 2, si + 3, si + 0]);
            }
            if !c.north {
                let si = vertices.len() as u32;
                let normal = [0., 0., -1.];
                vertices.push(make_vertex(
                    [x - hw, y - hh, z - hd],
                    color,
                    normal,
                    [bwf, bhf],
                    [0., 0.],
                ));
                vertices.push(make_vertex(
                    [x + hw, y - hh, z - hd],
                    color,
                    normal,
                    [bwf, bhf],
                    [bwf, 0.],
                ));
                vertices.push(make_vertex(
                    [x + hw, y + hh, z - hd],
                    color,
                    normal,
                    [bwf, bhf],
                    [bwf, bhf],
                ));
                vertices.push(make_vertex(
                    [x - hw, y + hh, z - hd],
                    color,
                    normal,
                    [bwf, bhf],
                    [0., bhf],
                ));
                indices.extend(&[si + 0, si + 1, si + 2, si + 2, si + 3, si + 0]);
            }
            if !c.south {
                let si = vertices.len() as u32;
                let normal = [0., 0., 1.];
                vertices.push(make_vertex(
                    [x - hw, y - hh, z + hd],
                    color,
                    normal,
                    [bwf, bhf],
                    [0., 0.],
                ));
                vertices.push(make_vertex(
                    [x - hw, y + hh, z + hd],
                    color,
                    normal,
                    [bwf, bhf],
                    [0., bhf],
                ));
                vertices.push(make_vertex(
                    [x + hw, y + hh, z + hd],
                    color,
                    normal,
                    [bwf, bhf],
                    [bwf, bhf],
                ));
                vertices.push(make_vertex(
                    [x + hw, y - hh, z + hd],
                    color,
                    normal,
                    [bwf, bhf],
                    [bwf, 0.],
                ));
                indices.extend(&[si + 0, si + 1, si + 2, si + 2, si + 3, si + 0]);
            }
            if !c.west {
                let si = vertices.len() as u32;
                let normal = [-1., 0., 0.];
                vertices.push(make_vertex(
                    [x - hw, y - hh, z - hd],
                    color,
                    normal,
                    [bhf, bdf],
                    [0., 0.],
                ));
                vertices.push(make_vertex(
                    [x - hw, y + hh, z - hd],
                    color,
                    normal,
                    [bhf, bdf],
                    [bhf, 0.],
                ));
                vertices.push(make_vertex(
                    [x - hw, y + hh, z + hd],
                    color,
                    normal,
                    [bhf, bdf],
                    [bhf, bdf],
                ));
                vertices.push(make_vertex(
                    [x - hw, y - hh, z + hd],
                    color,
                    normal,
                    [bhf, bdf],
                    [0., bdf],
                ));
                indices.extend(&[si + 0, si + 1, si + 2, si + 2, si + 3, si + 0]);
            }
            if !c.east {
                let si = vertices.len() as u32;
                let normal = [1., 0., 0.];
                vertices.push(make_vertex(
                    [x + hw, y - hh, z - hd],
                    color,
                    normal,
                    [bhf, bdf],
                    [0., 0.],
                ));
                vertices.push(make_vertex(
                    [x + hw, y - hh, z + hd],
                    color,
                    normal,
                    [bhf, bdf],
                    [0., bdf],
                ));
                vertices.push(make_vertex(
                    [x + hw, y + hh, z + hd],
                    color,
                    normal,
                    [bhf, bdf],
                    [bhf, bdf],
                ));
                vertices.push(make_vertex(
                    [x + hw, y + hh, z - hd],
                    color,
                    normal,
                    [bhf, bdf],
                    [bhf, 0.],
                ));
                indices.extend(&[si + 0, si + 1, si + 2, si + 2, si + 3, si + 0]);
            }
        }
        let tess = TessBuilder::new(ctx)
            .set_mode(Mode::Triangle)
            .add_vertices(&vertices[..])
            .set_indices(&indices[..])
            .build()
            .expect("could not build tess");
        self.bricks_tess = Some(tess);
    }
}
