#[cfg(feature = "backend_luminance")]
pub mod luminance;
#[cfg(feature = "backend_null")]
pub mod null;

pub fn available_backends() -> Vec<&'static str> {
    let mut backends = Vec::new();
    #[cfg(feature = "backend_luminance")]
    backends.push("luminance");
    #[cfg(feature = "backend_null")]
    backends.push("null");
    backends
}
