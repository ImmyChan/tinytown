use {
    image,
    luminance::{
        context::GraphicsContext,
        pixel::RGBA32F,
        texture::{Dim2, Sampler, Texture, GenMipmaps},
    },
    std::path::Path,
};

use crate::texture_manager::TextureLoader;

pub struct TexLoader<'a, C: GraphicsContext + 'a> {
    ctx: &'a mut C,
}

impl<'a, C: GraphicsContext + 'a> TexLoader<'a, C> {
    pub fn new(ctx: &'a mut C) -> TexLoader<'a, C> {
        TexLoader { ctx }
    }
}

impl<'a, C: GraphicsContext + 'a> TextureLoader for TexLoader<'a, C> {
    type Texture = Texture<Dim2, RGBA32F>;

    fn load_texture<P: AsRef<Path>>(&mut self, path: P) -> anyhow::Result<Self::Texture> {
        let path = path.as_ref();
        log::info!("loading texture {:?}", path);
        let img = image::open(path)?;
        let rgb_img = img.to_rgba();
        let (w, h) = rgb_img.dimensions();
        let texels = rgb_img
            .pixels()
            .map(|rgb| {
                (
                    rgb[0] as f32 / 255.,
                    rgb[1] as f32 / 255.,
                    rgb[2] as f32 / 255.,
                    rgb[3] as f32 / 255.,
                )
            })
            .collect::<Vec<_>>();
        let tex = Texture::new(self.ctx, [w, h], 0, Sampler::default()).expect("failed to create texture"); // TODO: throw
        tex.upload(GenMipmaps::Yes, &texels).expect("failed to upload texture data"); // TODO: throw
        Ok(tex)
    }
}
