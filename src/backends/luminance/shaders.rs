use {
    luminance::{
        pipeline::BoundTexture,
        pixel::Floating,
        shader::program::{Program, Uniform, BuiltProgram},
        texture::Dim2,
    },
    luminance_derive::{
        UniformInterface,
        Semantics,
        Vertex,
    },
    cgmath::{
        Point3,
        Point2,
        Vector3,
        Vector2,
    },
    std::fs,
};

use crate::{
    util::Color,
};

#[derive(UniformInterface)]
pub struct BrickShaderInterface {
    pub view: Uniform<[[f32; 4]; 4]>,
    pub projection: Uniform<[[f32; 4]; 4]>,
    pub model: Uniform<[[f32; 4]; 4]>,
    pub light: Uniform<[f32; 3]>,
    pub default_color: Uniform<[f32; 4]>,
    #[uniform(unbound)]
    pub tex_top: Uniform<&'static BoundTexture<'static, Dim2, Floating>>,
    #[uniform(unbound)]
    pub tex_side: Uniform<&'static BoundTexture<'static, Dim2, Floating>>,
    #[uniform(unbound)]
    pub tex_bottom: Uniform<&'static BoundTexture<'static, Dim2, Floating>>,
}

#[derive(UniformInterface)]
pub struct FloorShaderInterface {
    pub view: Uniform<[[f32; 4]; 4]>,
    pub projection: Uniform<[[f32; 4]; 4]>,
    pub model: Uniform<[[f32; 4]; 4]>,
    pub light: Uniform<[f32; 3]>,
    #[uniform(unbound)]
    pub tex: Uniform<&'static BoundTexture<'static, Dim2, Floating>>,
}

#[derive(Debug, Clone, Copy, Semantics)]
pub enum VertexSemantics {
    #[sem(name = "position", repr = "[f32; 3]", wrapper = "VertexPosition")]
    Position,
    #[sem(name = "color", repr = "[f32; 4]", wrapper = "VertexColor")]
    Color,
    #[sem(name = "normal", repr = "[f32; 3]", wrapper = "VertexNormal")]
    Normal,
    #[sem(name = "tex_size", repr = "[f32; 2]", wrapper = "VertexTexSize")]
    TexSize,
    #[sem(name = "tex_uv", repr = "[f32; 2]", wrapper = "VertexTexUv")]
    TexUv,
}

#[derive(Debug, Clone, Vertex)]
#[vertex(sem = "VertexSemantics")]
pub struct BrickVertex {
    pub position: VertexPosition,
    pub color: VertexColor,
    pub normal: VertexNormal,
    pub tex_size: VertexTexSize,
    pub tex_uv: VertexTexUv,
}

impl BrickVertex {
    pub fn create(position: Point3<f32>, color: Color, normal: Vector3<f32>, tex_size: Vector2<f32>, tex_uv: Point2<f32>) -> BrickVertex {
        BrickVertex {
            position: VertexPosition::new(position.into()),
            color: VertexColor::new(color.into()),
            normal: VertexNormal::new(normal.into()),
            tex_size: VertexTexSize::new(tex_size.into()),
            tex_uv: VertexTexUv::new(tex_uv.into()),
        }
    }
}

#[derive(Debug, Clone, Vertex)]
#[vertex(sem = "VertexSemantics")]
pub struct FloorVertex {
    pub position: VertexPosition,
    pub normal: VertexNormal,
    pub tex_uv: VertexTexUv,
}

impl FloorVertex {
    pub fn create(position: Point3<f32>, normal: Vector3<f32>, tex_uv: Point2<f32>) -> FloorVertex {
        FloorVertex {
            position: VertexPosition::new(position.into()),
            normal: VertexNormal::new(normal.into()),
            tex_uv: VertexTexUv::new(tex_uv.into()),
        }
    }
}

pub struct Shaders {
    pub bricks: Program<VertexSemantics, (), BrickShaderInterface>,
    pub floor: Program<VertexSemantics, (), FloorShaderInterface>,
}

impl Shaders {
    pub fn new() -> anyhow::Result<Shaders> {
        let brick_vertex_shader = fs::read_to_string("resources/shaders/brick_vertex.glsl")?;
        let brick_fragment_shader = fs::read_to_string("resources/shaders/brick_fragment.glsl")?;
        let floor_vertex_shader = fs::read_to_string("resources/shaders/floor_vertex.glsl")?;
        let floor_fragment_shader = fs::read_to_string("resources/shaders/floor_fragment.glsl")?;
        let bricks =
            Program::from_strings(None, &brick_vertex_shader, None, &brick_fragment_shader).expect("failed to load brick shader").program; // TODO: throw
        let floor =
            Program::from_strings(None, &floor_vertex_shader, None, &floor_fragment_shader).expect("failed to load floor shader").program; // TODO: throw
        Ok(Shaders {
            bricks,
            floor,
        })
    }
}
