use std::{
    collections::HashMap,
    ops::{Index, IndexMut},
    path::Path,
};

use super::{BrickData, BrickDataId};

use crate::loaders::brick_loader;

#[derive(Clone, Debug, Default)]
pub struct BrickDataStore {
    brick_datas: Vec<BrickData>,
    brick_data_id: HashMap<String, BrickDataId>,
}

impl BrickDataStore {
    pub fn new() -> BrickDataStore {
        BrickDataStore {
            brick_datas: Vec::new(),
            brick_data_id: HashMap::new(),
        }
    }

    pub fn add(&mut self, brick_data: BrickData) -> BrickDataId {
        if let Some(&id) = self.brick_data_id.get(&brick_data.name) {
            // TODO: maybe check equality?
            id
        } else {
            let id = BrickDataId(self.brick_datas.len());
            self.brick_data_id.insert(brick_data.name.clone(), id);
            self.brick_datas.push(brick_data);
            id
        }
    }

    pub fn load_brick<P: AsRef<Path>>(
        &mut self,
        path: P,
    ) -> Result<BrickDataId, brick_loader::BrickLoaderError> {
        let brick_data = brick_loader::load(path)?;
        let idx = self.add(brick_data);
        Ok(idx)
    }

    pub fn find_by_name(&self, name: &str) -> Option<BrickDataId> {
        self.brick_data_id.get(name).map(|x| *x)
    }

    pub fn iter(&self) -> impl Iterator<Item = (BrickDataId, &BrickData)> {
        self.brick_datas.iter().enumerate().map(|(i, bd)| (BrickDataId(i), bd))
    }

    pub fn next(&self, BrickDataId(mut brick_data_id): BrickDataId) -> BrickDataId {
        brick_data_id += 1;
        if brick_data_id >= self.brick_datas.len() {
            brick_data_id -= self.brick_datas.len();
        }
        BrickDataId(brick_data_id)
    }

    pub fn prev(&self, BrickDataId(brick_data_id): BrickDataId) -> BrickDataId {
        if brick_data_id == 0 {
            BrickDataId(self.brick_datas.len() - 1)
        }
        else {
            BrickDataId(brick_data_id - 1)
        }
    }
}

impl Index<BrickDataId> for BrickDataStore {
    type Output = BrickData;

    fn index(&self, BrickDataId(idx): BrickDataId) -> &Self::Output {
        &self.brick_datas[idx]
    }
}

impl IndexMut<BrickDataId> for BrickDataStore {
    fn index_mut(&mut self, BrickDataId(idx): BrickDataId) -> &mut Self::Output {
        &mut self.brick_datas[idx]
    }
}
