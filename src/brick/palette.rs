use std::ops::{Index, IndexMut};

use crate::util::Color;

#[derive(Clone, Copy, Debug, PartialEq, Eq, PartialOrd, Ord, Hash)]
pub struct BrickColorId(pub usize);

#[derive(Clone, Debug, Default)]
pub struct BrickPalette {
    pub colors: Vec<Color>,
}

impl BrickPalette {
    pub fn new() -> BrickPalette {
        BrickPalette { colors: Vec::new() }
    }

    pub fn next(&self, BrickColorId(mut color_id): BrickColorId) -> BrickColorId {
        color_id += 1;
        if color_id >= self.colors.len() {
            color_id -= self.colors.len();
        }
        BrickColorId(color_id)
    }

    pub fn prev(&self, BrickColorId(color_id): BrickColorId) -> BrickColorId {
        if color_id == 0 {
            BrickColorId(self.colors.len() - 1)
        }
        else {
            BrickColorId(color_id - 1)
        }
    }
}

impl Index<BrickColorId> for BrickPalette {
    type Output = Color;

    fn index(&self, BrickColorId(idx): BrickColorId) -> &Self::Output {
        &self.colors[idx]
    }
}

impl IndexMut<BrickColorId> for BrickPalette {
    fn index_mut(&mut self, BrickColorId(idx): BrickColorId) -> &mut Self::Output {
        &mut self.colors[idx]
    }
}
