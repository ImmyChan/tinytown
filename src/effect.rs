use crate::{
    loaders::effect_loader,
    texture_manager::{TextureId, TextureManagerProxy},
    util::Color,
};

use {
    cgmath::{prelude::*, Vector3},
    std::{
        f32::consts::PI,
        ops::Index,
        path::{Path, PathBuf},
    },
};

#[derive(Clone, Copy, Debug, PartialEq, PartialOrd, Eq, Ord, Hash)]
pub struct EffectId(usize);

#[derive(Clone)]
pub struct EffectStore {
    effects: Vec<(PathBuf, EffectData)>,
    proxy: TextureManagerProxy,
}

impl EffectStore {
    pub fn new(proxy: TextureManagerProxy) -> EffectStore {
        EffectStore {
            effects: Vec::new(),
            proxy,
        }
    }

    pub fn find_by_name(&self, name: &str) -> Option<EffectId> {
        self.effects
            .iter()
            .position(|&(_, ref eff)| eff.name == name)
            .map(EffectId)
    }

    pub fn load<P: AsRef<Path>>(&mut self, path: P) -> anyhow::Result<EffectId> {
        let path = path.as_ref();
        let effect_def = effect_loader::load(path)?;
        let effect = self.load_def(effect_def);
        let len = self.effects.len();
        self.effects.push((path.to_owned(), effect));
        Ok(EffectId(len))
    }

    fn load_def(&self, effect_def: effect_loader::EffectDef) -> EffectData {
        let mut emitters = Vec::new();
        for em in effect_def.emitters {
            let p = &effect_def.particles[&em.particle];
            let particle = ParticleData {
                texture: self.proxy.load(&p.texture),
                color: Color::rgba(
                    p.color.0 as f32 / 255.,
                    p.color.1 as f32 / 255.,
                    p.color.2 as f32 / 255.,
                    p.color.3 as f32 / 255.,
                ),
                accel: Vector3::from(p.accel),
                lifetime: p.lifetime,
            };
            let vel = Vector3::from(em.ejection_velocity);
            let vel_norm = vel.normalize();
            let vel_mag = vel.magnitude();
            let emitter = EmitterData {
                offset: Vector3::from(em.offset),
                radius: Vector3::from(em.radius),
                ejection_velocity: vel_mag,
                ejection_dir: vel_norm,
                ejection_spread: em.ejection_spread / 180. * PI,
                ejection_velocity_variance: em.ejection_velocity_variance,
                amount: em.amount,
                start_time: em.start_time,
                end_time: em.end_time,
                particle,
            };
            emitters.push(emitter);
        }
        EffectData {
            name: effect_def.effect.name,
            emitters,
        }
    }
}

impl Index<EffectId> for EffectStore {
    type Output = EffectData;

    fn index(&self, EffectId(idx): EffectId) -> &EffectData {
        &self.effects[idx].1
    }
}

#[derive(Clone, Debug)]
pub struct EffectData {
    pub name: String,
    pub emitters: Vec<EmitterData>,
}

#[derive(Clone, Debug)]
pub struct EmitterData {
    pub offset: Vector3<f32>,
    pub radius: Vector3<f32>,
    pub ejection_dir: Vector3<f32>,
    pub ejection_velocity: f32,
    pub ejection_spread: f32,
    pub ejection_velocity_variance: f32,
    pub amount: u32,
    pub start_time: f32,
    pub end_time: f32,
    pub particle: ParticleData,
}

#[derive(Clone, Debug)]
pub struct ParticleData {
    pub texture: TextureId,
    pub color: Color,
    pub accel: Vector3<f32>,
    pub lifetime: f32,
}
