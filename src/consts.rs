pub const NEAR: f32 = 0.1;
pub const FAR: f32 = 1000.;
pub const FOV_DEG: f32 = 45.;
pub const TOLERANCE: f32 = 0.00001;
pub const GJK_MAX_ITERATIONS: usize = 200;
#[cfg(not(test))]
pub const EPA_MAX_ITERATIONS: usize = 200;
#[cfg(test)]
pub const EPA_MAX_ITERATIONS: usize = 1000;
