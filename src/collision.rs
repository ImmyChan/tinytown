use {
    cgmath::{prelude::*, Point3, Vector3},
    shrev::EventChannel,
    specs::prelude::*,
    std::collections::HashMap,
};

mod gjk;
mod shape;
mod support;
#[cfg(test)]
mod tests;

pub use self::{
    gjk::{gjk, gjk_collide, gjk_cast_ray, Simplex},
    shape::{Midpoint, Shape, TransformedShape},
    support::{MinkowskiDifference, Support},
};

use crate::{
    util::{WorldAABB, Ray},
    math_ext::EuclidElementWise,
};

#[derive(Debug, Copy, Clone, PartialEq, Eq)]
pub struct CollisionMask(u8);

impl CollisionMask {
    pub const PHYSICAL: CollisionMask = CollisionMask(1);
    pub const BRICK: CollisionMask = CollisionMask(2);

    #[inline(always)]
    pub fn empty() -> CollisionMask {
        CollisionMask(0)
    }

    #[inline(always)]
    pub fn union(self, other: CollisionMask) -> CollisionMask {
        CollisionMask(self.0 | other.0)
    }

    #[inline(always)]
    pub fn intersect(self, other: CollisionMask) -> CollisionMask {
        CollisionMask(self.0 & other.0)
    }

    #[inline(always)]
    pub fn is_empty(self) -> bool {
        self.0 == 0
    }
}

const CELL_SIZE: Vector3<f32> = Vector3 {
    x: 6.,
    y: 6.,
    z: 6.,
};

fn cell_pos(wpos: Point3<f32>) -> Point3<i32> {
    Point3::from_vec(
        wpos.to_vec()
            .div_element_wise(CELL_SIZE)
            .map(|x| x.floor() as i32),
    )
}

#[derive(Clone, Debug)]
pub struct Collision {
    pub entity_a: Entity,
    pub entity_b: Entity,
    pub normal: Vector3<f32>,
    pub depth: f32,
}

pub struct CollisionManager {
    cells: HashMap<Point3<i32>, Vec<(Entity, CollisionMask, WorldAABB, TransformedShape)>>,
    pub events: EventChannel<Collision>,
}

impl Default for CollisionManager {
    fn default() -> CollisionManager {
        CollisionManager {
            cells: HashMap::new(),
            events: EventChannel::new(),
        }
    }
}

impl CollisionManager {
    pub fn clear(&mut self) {
        self.cells.clear();
    }

    pub fn add(&mut self, aabb: WorldAABB, mask: CollisionMask, shape: TransformedShape, entity: Entity) {
        let bsw_cell_wpos = aabb.bsw_pos();
        let une_cell_wpos = aabb.une_pos();
        let bsw_cell_cpos = cell_pos(bsw_cell_wpos);
        let une_cell_cpos = cell_pos(une_cell_wpos);
        for iy in bsw_cell_cpos.y..une_cell_cpos.y + 1 {
            for ix in bsw_cell_cpos.x..une_cell_cpos.x + 1 {
                for iz in bsw_cell_cpos.z..une_cell_cpos.z + 1 {
                    self.cells
                        .entry(Point3::new(ix, iy, iz))
                        .or_insert(Vec::with_capacity(2))
                        .push((entity, mask, aabb, shape));
                }
            }
        }
    }

    pub fn cells<'a>(&'a self) -> impl Iterator<Item = &'a Vec<(Entity, CollisionMask, WorldAABB, TransformedShape)>> {
        self.cells.values()
    }

    pub fn cast_ray(&self, mask: CollisionMask, ray: Ray) -> Option<RaycastHit> {
        let mut cur = cell_pos(ray.origin);
        let step = ray.dir.map(|x| x.signum()).cast::<i32>().unwrap();
        let mut t_max = ray.origin.to_vec().rem_euclid_element_wise(CELL_SIZE);
        if step.x == 1 { t_max.x = CELL_SIZE.x - t_max.x; }
        if step.y == 1 { t_max.y = CELL_SIZE.y - t_max.y; }
        if step.z == 1 { t_max.z = CELL_SIZE.z - t_max.z; }
        let dir_abs = ray.dir.map(|x| x.abs());
        t_max = t_max.div_element_wise(dir_abs);
        let t_delta = CELL_SIZE.div_element_wise(dir_abs);
        for _ in 0..16 { // distance limit; TODO: might want to specify this in terms of a different unit than cells
            if t_max.x < t_max.y {
                if t_max.x < t_max.z {
                    cur.x += step.x;
                    t_max.x += t_delta.x;
                }
                else {
                    cur.z += step.z;
                    t_max.z += t_delta.z;
                }
            }
            else if t_max.y < t_max.z {
                cur.y += step.y;
                t_max.y += t_delta.y;
            }
            else {
                cur.z += step.z;
                t_max.z += t_delta.z;
            }
            if cur.y < 0 {
                if let Some(t) = ray.intersects_floor() {
                    let point = ray.point_at(t);
                    let normal = Vector3::new(0., 1., 0.);
                    return Some(RaycastHit {
                        point,
                        normal,
                        distance: t,
                        entity: None,
                    });
                }
                else {
                    return None;
                }
            }
            if let Some(cell) = self.cells.get(&cur) {
                let mut best = None;
                let mut best_t = 9999.;
                for &(entity, entity_mask, world_aabb, shape) in cell {
                    if !mask.intersect(entity_mask).is_empty() {
                        if let Some(t) = world_aabb.cast_ray(ray) {
                            if best.is_none() || t < best_t {
                                let p = ray.point_at(t);
                                let n = world_aabb.normal_at(p);
                                // TODO: use gjk
                                best = Some(RaycastHit {
                                    point: p,
                                    normal: n,
                                    distance: t,
                                    entity: Some(entity),
                                });
                                best_t = t;
                                //if let Some((hit_point, hit_normal)) = gjk_cast_ray(origin, dir, &shape) {
                                //    return Some(RaycastHit {
                                //        point: hit_point,
                                //        normal: hit_normal,
                                //        entity: Some(entity),
                                //    });
                                //}
                            }
                        }
                    }
                }
                if let Some(hit) = best {
                    return Some(hit);
                }
            }
        }
        None
    }
}

#[derive(Clone, Copy, Debug, PartialEq)]
pub struct RaycastHit {
    pub point: Point3<f32>,
    pub normal: Vector3<f32>,
    pub distance: f32,
    pub entity: Option<Entity>,
}

#[derive(Clone, Copy, Debug, PartialEq)]
pub struct CollisionInfo {
    pub normal: Vector3<f32>,
    pub depth: f32,
}
