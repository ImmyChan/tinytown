use {
    cgmath::{Point3, Vector3},
    specs::{
        prelude::*,
        self,
        Builder,
    },
    std::{
        fs::File,
        io::{self, BufRead, BufReader},
        path::Path,
    },
};

use crate::{
    brick::{BrickColorId, BrickData, BrickDataStore, BrickOrientation, BrickPalette, VOXEL_SIZE},
    components::Brick,
    util::{self, Color, Directional},
};

// FIXME: too much unwrapping

fn add_default_brick_types(bds: &mut BrickDataStore) {
    macro_rules! add {
        ($name:expr, $size:expr) => {
            let brick_data = BrickData {
                name: $name.to_owned(),
                size: $size,
            };
            bds.add(brick_data);
        };
    }
    add!("16x16 Base", Vector3::new(16, 1, 16));
    add!("16x32 Base", Vector3::new(16, 1, 32));
    add!("32x32 Base", Vector3::new(32, 1, 32));
    add!("48x48 Base", Vector3::new(48, 1, 48));
    add!("64x64 Base", Vector3::new(64, 1, 64));
    add!("32x32 Road", Vector3::new(32, 1, 32));
    add!("32x32 Road X", Vector3::new(32, 1, 32));
    add!("32x32 Road T", Vector3::new(32, 1, 32));
    add!("32x32 Road C", Vector3::new(32, 1, 32));
}

pub fn load_bls_into<P: AsRef<Path>>(path: P, world: &mut specs::World) -> Result<(), io::Error> {
    let fd = File::open(path)?;
    let reader = BufReader::new(fd);
    let mut lines = reader.lines();
    let _comment = lines.next().unwrap()?;
    let _no_idea_1 = lines.next().unwrap()?;
    let _no_idea_2 = lines.next().unwrap()?;
    let previous_palette_len;
    {
        let mut palette = world.write_resource::<BrickPalette>();
        previous_palette_len = palette.colors.len();
        for _ in 0..64 {
            let line = lines.next().unwrap()?;
            let mut sp = line.split(' ');
            let r: f32 = sp.next().unwrap().parse().unwrap();
            let g: f32 = sp.next().unwrap().parse().unwrap();
            let b: f32 = sp.next().unwrap().parse().unwrap();
            let a: f32 = sp.next().unwrap().parse().unwrap();
            palette.colors.push(Color::rgba(r, g, b, a));
        }
    }
    let _n_bricks_str = lines.next().unwrap()?;
    let mut bricks = Vec::new();
    {
        let mut bds = world.write_resource::<BrickDataStore>();
        add_default_brick_types(&mut bds);
        for line in lines {
            let line = line?;
            if line.starts_with("+-") {
                continue;
            }
            let quote_idx = line.find('"').unwrap();
            let (name, rest) = line.split_at(quote_idx);
            let mut sp = rest.split(' ');
            assert_eq!(sp.next().unwrap(), "\"");
            let cx: f32 = sp.next().unwrap().parse().unwrap();
            let cz: f32 = sp.next().unwrap().parse().unwrap();
            let cy: f32 = sp.next().unwrap().parse().unwrap();
            let orient: u8 = sp.next().unwrap().parse().unwrap();
            let _idk_2 = sp.next().unwrap();
            let color_id: usize = sp.next().unwrap().parse().unwrap();
            let data_id = if let Some(id) = bds.find_by_name(name) {
                id
            } else if let Some(brick_data) = name_to_brick_data(name) {
                bds.add(brick_data)
            } else {
                eprintln!("Cannot create brick type: {}", name);
                continue;
            };
            let brick_data = &bds[data_id];
            let size = brick_data.size;
            let (bw, bh, bd) = (size.x, size.y, size.z);
            let (w, h, d) = (
                bw as f32 * VOXEL_SIZE.x,
                bh as f32 * VOXEL_SIZE.y,
                bd as f32 * VOXEL_SIZE.z,
            );
            let wx;
            let wy = cy - h / 2. + 0.001;
            let wz;
            if orient % 2 == 0 {
                wx = cx - w / 2.;
                wz = cz - d / 2.;
            } else {
                wx = cx - d / 2.;
                wz = cz - w / 2.;
            }
            let pos = util::world_to_brick(Point3::new(wx, wy, wz));
            bricks.push((
                pos,
                data_id,
                BrickColorId(previous_palette_len + color_id),
                BrickOrientation(orient),
            ));
        }
    }
    for (pos, data_id, color_id, orient) in bricks {
        let brick = Brick {
            pos: pos,
            data_id,
            color_id,
            orientation: orient,
            covered: Directional::all(false),
        };
        world.create_entity().with(brick).build();
    }
    Ok(())
}

fn name_to_brick_data(mut name: &str) -> Option<BrickData> {
    if let Some(idx) = name.find(' ') {
        let (value, _rest) = name.split_at(idx);
        name = value;
    }
    let mut sp = name.split('x');
    let first = sp.next()?;
    let second = sp.next()?;
    let w = first.parse().ok()?;
    let mut h = 3;
    let d;
    if second.ends_with('F') {
        d = second[..second.len() - 1].parse().ok()?; // FIXME: UNICODE!!! (though not like this format seems to care…)
        h = 1;
    } else {
        d = second.parse().ok()?;
        if let Some(third) = sp.next() {
            h = third.parse().ok()?;
            h *= 3;
        }
    }
    Some(BrickData {
        name: name.to_owned(),
        size: Vector3::new(w, h, d),
    })
}
