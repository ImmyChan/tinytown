use {
    serde::{Deserialize, Serialize},
    std::collections::HashMap,
    std::{
        fs,
        path::{Path, PathBuf},
    },
    toml,
};

fn zero() -> f32 {
    0.
}

fn one() -> f32 {
    1.
}

fn zero_vector() -> (f32, f32, f32) {
    (0., 0., 0.)
}

fn full_color() -> (u8, u8, u8, u8) {
    (255, 255, 255, 255)
}

#[derive(Clone, Debug, Serialize, Deserialize)]
pub struct EffectDef {
    pub effect: EffectMetaDef,
    pub emitters: Vec<EmitterDef>,
    pub particles: HashMap<String, ParticleDef>,
}

#[derive(Clone, Debug, Serialize, Deserialize)]
pub struct EffectMetaDef {
    pub name: String,
}

#[derive(Clone, Debug, Serialize, Deserialize)]
pub struct EmitterDef {
    pub particle: String,
    pub amount: u32,
    #[serde(default = "zero_vector")]
    pub offset: (f32, f32, f32),
    #[serde(default = "zero_vector")]
    pub radius: (f32, f32, f32),
    #[serde(default = "zero_vector")]
    pub ejection_velocity: (f32, f32, f32),
    #[serde(default = "zero")]
    pub ejection_spread: f32,
    #[serde(default = "zero")]
    pub ejection_velocity_variance: f32,
    #[serde(default = "zero")]
    pub start_time: f32,
    #[serde(default = "zero")]
    pub end_time: f32,
}

#[derive(Clone, Debug, Serialize, Deserialize)]
pub struct ParticleDef {
    pub texture: PathBuf,
    pub lifetime: f32,
    #[serde(default = "full_color")]
    pub color: (u8, u8, u8, u8),
    #[serde(default = "zero_vector")]
    pub accel: (f32, f32, f32),
}

pub fn load<P: AsRef<Path>>(path: P) -> anyhow::Result<EffectDef> {
    let data = fs::read_to_string(path)?;
    let effect: EffectDef = toml::from_str(&data)?;
    for emitter in &effect.emitters {
        assert!(emitter.ejection_spread <= 180.);
        assert!(emitter.ejection_velocity_variance >= 0.);
        assert!(emitter.end_time >= emitter.start_time);
        assert!(effect.particles.contains_key(&emitter.particle));
    }
    for particle in effect.particles.values() {
        assert!(particle.lifetime >= 0.);
    }
    Ok(effect)
}
