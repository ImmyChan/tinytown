use crate::brick::BrickData;

use {
    thiserror::Error,
    serde::{Deserialize, Serialize},
    std::{
        fs, io,
        path::{Path, PathBuf},
    },
    toml,
};

#[derive(Serialize, Deserialize)]
struct BrickDefinitionFile {
    brick: BrickDefinition,
    model: Option<BrickModelDefinition>,
}

#[derive(Serialize, Deserialize)]
struct BrickDefinition {
    name: String,
    size: (u32, u32, u32),
}

#[derive(Serialize, Deserialize)]
struct BrickModelDefinition {
    path: PathBuf,
}

#[derive(Debug, Error)]
pub enum BrickLoaderError {
    #[error("IO error: {0:}")]
    IoError(#[from] io::Error),

    #[error("TOML error: {0:}")]
    TomlError(#[from] toml::de::Error),
}

pub fn load<P: AsRef<Path>>(path: P) -> Result<BrickData, BrickLoaderError> {
    let data = fs::read_to_string(path)?;
    let bdf: BrickDefinitionFile = toml::from_str(&data)?;
    if bdf.model.is_some() {
        panic!("Brick models are currently unsupported!");
    }
    Ok(BrickData {
        name: bdf.brick.name,
        size: bdf.brick.size.into(),
    })
}
