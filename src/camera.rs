use cgmath::{prelude::*, Matrix4, PerspectiveFov, Point3, Vector3};

#[derive(Clone, Debug)]
pub struct Camera {
    pub eye: Point3<f32>,
    pub dir: Vector3<f32>,
    pub up: Vector3<f32>,
    pub perspective_fov: PerspectiveFov<f32>,
}

impl Camera {
    pub fn new(
        eye: Point3<f32>,
        dir: Vector3<f32>,
        up: Vector3<f32>,
        perspective_fov: PerspectiveFov<f32>,
    ) -> Camera {
        Camera {
            eye,
            dir: dir.normalize(),
            up,
            perspective_fov,
        }
    }

    pub fn projection(&self) -> Matrix4<f32> {
        let perspective = self.perspective_fov.to_perspective();

        Matrix4::from(perspective)
    }

    pub fn view(&self) -> Matrix4<f32> {
        Matrix4::look_at_dir(self.eye, self.dir, self.up)
    }

    pub fn look_at(&mut self, target: Point3<f32>) {
        self.dir = (target - self.eye).normalize();
    }
}
