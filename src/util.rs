use crate::brick::VOXEL_SIZE;

use cgmath::Point3;

mod aabb;
mod color;
mod directional;
mod ray;

pub use self::{
    aabb::{BrickAABB, WorldAABB},
    color::Color,
    directional::Directional,
    ray::Ray,
};

pub fn brick_to_world(brick_pos: Point3<i32>) -> Point3<f32> {
    Point3::new(
        brick_pos.x as f32 * VOXEL_SIZE.x,
        brick_pos.y as f32 * VOXEL_SIZE.y,
        brick_pos.z as f32 * VOXEL_SIZE.z,
    )
}

pub fn world_to_brick(world_pos: Point3<f32>) -> Point3<i32> {
    Point3::new(
        (world_pos.x / VOXEL_SIZE.x).floor() as i32,
        (world_pos.y / VOXEL_SIZE.y).floor() as i32,
        (world_pos.z / VOXEL_SIZE.z).floor() as i32,
    )
}
