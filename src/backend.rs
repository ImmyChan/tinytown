use crate::texture_manager::{TextureId, TextureManagerProxy};

#[derive(Clone, Debug)]
pub struct BrickTextures {
    pub top: TextureId,
    pub side: TextureId,
    pub bottom: TextureId,
}

impl BrickTextures {
    pub fn new(proxy: &TextureManagerProxy) -> BrickTextures {
        let top = proxy.load("textures/brick_top.png");
        let side = proxy.load("textures/brick_top.png");
        let bottom = proxy.load("textures/brick_top.png");
        BrickTextures { top, side, bottom }
    }
}

pub trait BackendName {
    fn name() -> &'static str;
}

pub trait Backend {
    fn handle_events(&mut self, world: &specs::World) -> anyhow::Result<()>;
    fn render(&mut self, world: &specs::World) -> anyhow::Result<()>;
    fn aspect_ratio(&self) -> f32;
    fn size(&self) -> (u32, u32);
    fn texture_manager_proxy(&self) -> TextureManagerProxy;
}
