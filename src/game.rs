use {
    cgmath::{prelude::*, PerspectiveFov, Point3, Quaternion, Rad, Vector3},
    specs::prelude::*,
    std::{fs, io, path::Path, time::Instant},
};

use crate::{
    backend::Backend,
    brick::{BrickEventQueue, BrickDataStore, BrickPalette, BrickDataId, BrickColorId, BrickOrientation},
    camera::Camera,
    collision::{CollisionManager, Shape},
    components::{Brick, GhostBrick, Physical, Player},
    consts::{FAR, FOV_DEG, NEAR},
    controls::{Control, Controls},
    effect::EffectStore,
    loaders::blockland_loader,
    resources::{Stats, WindowSize},
    systems::{
        BrickCollisionResolver, CameraSystem, CollisionSystem, MovementSystem, PhysicsSystem,
        BuildingSystem, BrickSpawnerSystem,
    },
};

pub struct Game<B: Backend> {
    world: World,
    backend: B,
}

impl<B: Backend> Game<B> {
    pub fn new(backend: B) -> Game<B> {
        Game {
            world: World::new(),
            backend,
        }
    }

    pub fn init_camera(&mut self) {
        let perspective_fov = PerspectiveFov {
            near: NEAR,
            far: FAR,
            fovy: Rad(FOV_DEG * ::std::f32::consts::PI / 180.),
            aspect: self.backend.aspect_ratio(),
        };
        let mut camera = Camera::new(
            Point3::new(0., 10., 0.),
            Vector3::new(0., 0., 1.),
            Vector3::new(0., 1., 0.),
            perspective_fov,
        );
        camera.look_at(Point3::new(10., 0.3, 10.));
        self.world.insert(camera);
    }

    pub fn load_bls<P: AsRef<Path>>(&mut self, path: P) -> Result<(), io::Error> {
        blockland_loader::load_bls_into(path, &mut self.world)
    }

    pub fn init_components(&mut self) {
        self.world.register::<Brick>();
        self.world.register::<GhostBrick>();
        self.world.register::<Player>();
        self.world.register::<Physical>();
    }

    pub fn init_resources(&mut self) {
        self.world.insert(BrickDataStore::new());
        self.world.insert(CollisionManager::default());
        self.world.insert(Controls::new());
        self.world.insert(BrickPalette::new());
        self.world
            .insert(EffectStore::new(self.backend.texture_manager_proxy()));
        self.world.insert(BrickEventQueue::new());
        self.world.insert(Stats::new());
        let (width, height) = self.backend.size();
        self.world.insert(WindowSize { width, height });
        self.init_camera();
    }

    pub fn load_bricks<P: AsRef<Path>>(&mut self, path: P) -> anyhow::Result<()> {
        let mut bds = self.world.write_resource::<BrickDataStore>();
        for entry in fs::read_dir(path)? {
            let entry = entry?;
            let path = entry.path();
            if path.is_file() {
                bds.load_brick(path)?;
            }
        }
        Ok(())
    }

    pub fn load_effects<P: AsRef<Path>>(&mut self, path: P) -> anyhow::Result<()> {
        let mut effects = self.world.write_resource::<EffectStore>();
        for entry in fs::read_dir(path)? {
            let entry = entry?;
            let path = entry.path();
            if path.is_file() {
                effects.load(path)?;
            }
        }
        Ok(())
    }

    pub fn init(&mut self) -> anyhow::Result<()> {
        self.init_components();
        self.init_resources();
        self.load_bricks("bricks")?;
        self.load_effects("effects")?;
        self.world
            .create_entity()
            .with(Player::new(Vector3::new(1., 0., 0.)))
            .with(Physical::new(
                Point3::new(0., 50., 0.),
                Quaternion::one(),
                Shape::Rectangle(Vector3::new(0.5, 1., 0.5)),
            ))
            .build();
        Ok(())
    }

    pub fn start(&mut self) -> anyhow::Result<()> {
        let mut dispatcher = DispatcherBuilder::new()
            .with(MovementSystem, "movement", &[])
            .with(PhysicsSystem, "physics", &["movement"])
            .with(CollisionSystem, "collision", &["physics"])
            .with(BuildingSystem::new(&self.world), "building", &["collision"])
            .with(
                BrickCollisionResolver::new(&self.world),
                "brick_collision_resolver",
                &["collision"],
            )
            .with_barrier()
            .with(CameraSystem, "camera", &[])
            .with_barrier()
            .with(BrickSpawnerSystem, "brick_spawner", &[])
            .build();

        'app: loop {
            self.backend.handle_events(&self.world)?;
            dispatcher.dispatch(&mut self.world);
            self.world.maintain();
            let start_render = Instant::now();
            self.backend.render(&self.world)?;
            self.world
                .write_resource::<Stats>()
                .render_time
                .push(start_render.elapsed().subsec_nanos() / 1000_000);
            {
                let controls = self.world.read_resource::<Controls>();
                if controls.activated(Control::Exit) || controls.exit_requested() {
                    break 'app;
                }
            }
            self.world.read_resource::<Stats>().print_stats();
        }

        Ok(())
    }
}
