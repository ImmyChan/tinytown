use {
    std::{
        mem,
    },
    specs::{
        prelude::*,
    },
    cgmath::{
        prelude::*,
        Quaternion,
        Vector3,
        Point3,
    },
    shrev::{
        ReaderId,
    },
};

use crate::{
    controls::{
        Controls,
        Control,
        Action,
    },
    collision:: {
        CollisionManager,
        CollisionMask,
    },
    camera::{
        Camera,
    },
    components::{
        Brick,
        GhostBrick,
        Player,
    },
    brick::{
        VOXEL_SIZE,
        BrickEventQueue,
        BrickPalette,
        BrickDataStore,
        BrickDataId,
        BrickColorId,
        BrickOrientation,
    },
    util::{
        self,
        Ray,
    },
};

fn largest_component(v: Vector3<f32>) -> Vector3<i32> {
    if v.x > v.y {
        if v.x > v.z {
            Vector3::new(v.x.signum() as i32, 0, 0)
        }
        else {
            Vector3::new(0, 0, v.z.signum() as i32)
        }
    }
    else if v.y > v.z {
        Vector3::new(0, v.y.signum() as i32, 0)
    }
    else {
        Vector3::new(0, 0, v.z.signum() as i32)
    }
}

pub struct BuildingSystem {
    reader: ReaderId<(Action, Control)>,
}

impl BuildingSystem {
    pub fn new(world: &World) -> BuildingSystem {
        let mut controls: WriteExpect<Controls> = world.system_data();
        let reader = controls.events.register_reader();
        BuildingSystem {
            reader,
        }
    }
}

impl<'a> System<'a> for BuildingSystem {
    type SystemData = (
        Entities<'a>,
        ReadExpect<'a, Controls>,
        ReadExpect<'a, Camera>,
        ReadExpect<'a, CollisionManager>,
        ReadExpect<'a, BrickPalette>,
        ReadExpect<'a, BrickDataStore>,
        WriteExpect<'a, BrickEventQueue>,
        WriteStorage<'a, GhostBrick>,
        WriteStorage<'a, Brick>,
        WriteStorage<'a, Player>,
        ReadExpect<'a, LazyUpdate>,
    );

    fn run(&mut self, (entities, controls, camera, cm, bp, bds, mut beq, mut ghost_bricks, mut bricks, mut players, lazy): Self::SystemData) {
        // TODO: check that it's the local player
        for player in (&mut players).join() {
            if controls.activated(Control::Interact) {
                if let Some(hit) = cm.cast_ray(CollisionMask::BRICK, Ray::new(camera.eye, camera.dir)) {
                    let pos = util::world_to_brick(hit.point + hit.normal.mul_element_wise(VOXEL_SIZE * 0.25));
                    if let Some(gb) = player.ghost_brick.and_then(|gb_entity| ghost_bricks.get_mut(gb_entity)) {
                        gb.pos = pos;
                    }
                    else {
                        let gb_entity = lazy.create_entity(&entities)
                            .with(GhostBrick {
                                data_id: BrickDataId(0),
                                color_id: BrickColorId(0),
                                orientation: BrickOrientation(0),
                                pos,
                                visual_pos: util::brick_to_world(pos),
                                visual_rot: BrickOrientation(0).to_quaternion(),
                            })
                            .build();
                        player.ghost_brick = Some(gb_entity);
                    }
                }
            }
            let forward = Vector3::new(camera.dir.x, 0., camera.dir.z);
            if let Some(gb) = player.ghost_brick.and_then(|gb_entity| ghost_bricks.get_mut(gb_entity)) {
                let mut ghost_size = bds[gb.data_id].size;
                if gb.orientation.0 % 2 == 1 {
                    mem::swap(&mut ghost_size.x, &mut ghost_size.z);
                }
                let target_pos = util::brick_to_world(gb.pos).to_vec() + VOXEL_SIZE.mul_element_wise(ghost_size.cast::<f32>().unwrap()) * 0.5;
                let target_rot = gb.orientation.to_quaternion();
                gb.visual_pos = Point3::from_vec(gb.visual_pos.to_vec() * 0.2 + target_pos * 0.8);
                gb.visual_rot = gb.visual_rot.nlerp(target_rot, 0.8);
                let brick_fwd = if camera.dir.x.abs() > camera.dir.z.abs() {
                    Vector3::new(camera.dir.x.signum() as i32, 0, 0)
                } else {
                    Vector3::new(0, 0, camera.dir.z.signum() as i32)
                };
                let brick_ltl = Vector3::new(-brick_fwd.z, 0, brick_fwd.x);
                let ghost_speed = if controls.activated(Control::MoveGhostProportional) {
                    bds[gb.data_id].size.cast::<i32>().unwrap()
                } else {
                    Vector3::new(1, 1, 1)
                };
                for (action, control) in controls.events.read(&mut self.reader) {
                    if *action != Action::Press {
                        continue;
                    }
                    match *control {
                        Control::Build => {
                            beq.build(gb.data_id, gb.color_id, gb.orientation, gb.pos);
                        }
                        Control::MoveGhostForward => {
                            gb.pos += brick_fwd.mul_element_wise(ghost_speed);
                        }
                        Control::MoveGhostBackward => {
                            gb.pos -= brick_fwd.mul_element_wise(ghost_speed);
                        }
                        Control::MoveGhostRight => {
                            gb.pos += brick_ltl.mul_element_wise(ghost_speed);
                        }
                        Control::MoveGhostLeft => {
                            gb.pos -= brick_ltl.mul_element_wise(ghost_speed);
                        }
                        Control::MoveGhostUp => {
                            gb.pos += Vector3::new(0, 1, 0).mul_element_wise(ghost_speed);
                        }
                        Control::MoveGhostDown => {
                            gb.pos -= Vector3::new(0, 1, 0).mul_element_wise(ghost_speed);
                        }
                        Control::PalettePrev => {
                            gb.color_id = bp.prev(gb.color_id);
                        }
                        Control::PaletteNext => {
                            gb.color_id = bp.next(gb.color_id);
                        }
                        Control::CycleGhostPrev => {
                            gb.data_id = bds.prev(gb.data_id);
                        },
                        Control::CycleGhostNext => {
                            gb.data_id = bds.next(gb.data_id);
                        },
                        Control::RotateGhostCCW => {
                            gb.orientation = gb.orientation.prev();
                        },
                        Control::RotateGhostCW => {
                            gb.orientation = gb.orientation.next();
                        },
                        Control::DispelGhost => {
                            entities.delete(player.ghost_brick.unwrap());
                            player.ghost_brick = None;
                            break;
                        },
                        _ => ()
                    }
                }
            }
        }
    }
}
