use {
    cgmath::{InnerSpace, Matrix4, Vector2, Vector3},
    specs::prelude::*,
};

use crate::{
    camera::Camera,
    components::{Physical, Player},
    controls::{Control, Controls},
    resources::WindowSize,
};

const MOVE_SPEED_FORWARD: f32 = 0.03;
const MOVE_SPEED_BACKWARD: f32 = 0.02;
const MOVE_SPEED_SIDEWAYS: f32 = 0.01;

pub struct MovementSystem;

impl<'a> System<'a> for MovementSystem {
    type SystemData = (
        WriteStorage<'a, Player>,
        WriteStorage<'a, Physical>,
        ReadExpect<'a, Controls>,
        ReadExpect<'a, WindowSize>,
        ReadExpect<'a, Camera>,
    );

    fn run(
        &mut self,
        (mut players, mut physicals, controls, window_size, camera): Self::SystemData,
    ) {
        let up = Vector3::new(0., 1., 0.);
        let view_speed = 25.;
        for (player, physical) in (&mut players, &mut physicals).join() {
            let mut vel = Vector3::new(0., 0., 0.);
            let forward = Vector3::new(player.facing.x, 0., player.facing.z);
            let lateral = Vector3::new(-player.facing.z, 0., player.facing.x);
            if controls.activated(Control::Jump) && player.can_jump {
                vel += up * 2.;
            }
            if controls.activated(Control::MoveForward) {
                vel += forward * MOVE_SPEED_FORWARD;
            }
            if controls.activated(Control::MoveBackward) {
                vel -= forward * MOVE_SPEED_BACKWARD;
            }
            if controls.activated(Control::MoveRight) {
                vel += lateral * MOVE_SPEED_SIDEWAYS;
            }
            if controls.activated(Control::MoveLeft) {
                vel -= lateral * MOVE_SPEED_SIDEWAYS;
            }
            if controls.activated(Control::Jetpack) {
                vel += up * 0.3;
            }
            physical.vel += vel;
            if let (Some(last_cursor_pos), Some(cursor_pos)) =
                (controls.last_cursor_pos(), controls.cursor_pos())
            {
                let width = window_size.width as f32;
                let height = window_size.height as f32;
                let dpos = cursor_pos - last_cursor_pos;
                let rdpos = Vector2::new(dpos.x / width, dpos.y / height);
                let fovy = camera.perspective_fov.fovy;
                let fovx = camera.perspective_fov.fovy * camera.perspective_fov.aspect;
                let angley = -fovy / 2. * rdpos.y;
                let anglex = -fovx / 2. * rdpos.x;
                let camera_lateral = player.facing.cross(up).normalize();
                let rot = Matrix4::from_axis_angle(up, anglex * view_speed)
                    * Matrix4::from_axis_angle(camera_lateral, angley * view_speed);
                player.facing = (rot * player.facing.extend(0.)).truncate();
            }
            player.can_jump = false;
        }
    }
}
