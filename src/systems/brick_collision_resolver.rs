use {
    cgmath::{prelude::*, Vector3},
    shrev::ReaderId,
    specs::prelude::*,
};

use crate::{
    collision::{Collision, CollisionManager, CollisionMask},
    components::{Brick, Physical, Player},
    util::Ray,
};

pub struct BrickCollisionResolver {
    reader_id: ReaderId<Collision>,
}

impl BrickCollisionResolver {
    pub fn new(world: &World) -> BrickCollisionResolver {
        let reader_id = world
            .write_resource::<CollisionManager>()
            .events
            .register_reader();
        BrickCollisionResolver { reader_id }
    }
}

impl<'a> System<'a> for BrickCollisionResolver {
    type SystemData = (
        ReadExpect<'a, CollisionManager>,
        ReadStorage<'a, Brick>,
        WriteStorage<'a, Physical>,
        WriteStorage<'a, Player>,
    );

    fn run(&mut self, (collision_manager, bricks, mut physicals, mut players): Self::SystemData) {
        for collision in collision_manager.events.read(&mut self.reader_id) {
            let mut collision = collision.clone();
            if bricks.contains(collision.entity_a) {
                let temp = collision.entity_a;
                collision.entity_a = collision.entity_b;
                collision.entity_b = temp;
                collision.normal = -collision.normal;
            }
            let physical = physicals.get_mut(collision.entity_a).unwrap();
            physical.vel -= physical.vel.project_on(collision.normal);
            physical.pos -= collision.normal * collision.depth;
            if let Some(p) = players.get_mut(collision.entity_a) {
                if collision.normal.dot(Vector3::new(0., -1., 0.)) > 0.5 {
                    p.can_jump = true;
                }
            }
        }
    }
}
