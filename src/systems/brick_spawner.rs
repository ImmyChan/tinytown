
use {
    specs::{
        prelude::*,
    },
};

use crate::{
    components::{
        Brick,
        GhostBrick,
    },
    brick::{
        BrickEventQueue,
        BrickEvent,
    },
    util::Directional,
};

pub struct BrickSpawnerSystem;

impl<'a> System<'a> for BrickSpawnerSystem {
    type SystemData = (
        Entities<'a>,
        WriteExpect<'a, BrickEventQueue>,
        WriteStorage<'a, Brick>,
    );

    fn run(&mut self, (entities, mut beq, mut bricks): Self::SystemData) {
        beq.flush(|evt| match evt {
            BrickEvent::Build { data_id, color_id, orientation, pos } => {
                // TODO: maybe disallow bricks to intersect?
                entities.build_entity()
                    .with(Brick {
                        data_id,
                        color_id,
                        orientation,
                        pos,
                        covered: Directional::all(false),
                    }, &mut bricks)
                    .build();
            },
            BrickEvent::Destroy { entity } => {
                // TODO
            },
        });
    }
}
