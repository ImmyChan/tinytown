use {
    cgmath::{prelude::*, Quaternion},
    specs::prelude::*,
    std::{
        cmp::{Eq, PartialEq},
        collections::HashSet,
        hash::{Hash, Hasher},
        time::Instant,
    },
};

use crate::{
    brick::BrickDataStore,
    collision::{gjk_collide, Collision, CollisionManager, Shape, CollisionMask},
    components::{Brick, Physical},
    resources::Stats,
    util::{BrickAABB, WorldAABB},
};

pub struct CollisionSystem;

impl<'a> System<'a> for CollisionSystem {
    type SystemData = (
        Write<'a, CollisionManager>,
        ReadExpect<'a, BrickDataStore>,
        WriteExpect<'a, Stats>,
        Entities<'a>,
        ReadStorage<'a, Physical>,
        ReadStorage<'a, Brick>,
    );

    fn run(
        &mut self,
        (mut collision_manager, bds, mut stats, entities, physicals, bricks): Self::SystemData,
    ) {
        // TODO: update indexes, don't rebuild them
        let start_broad = Instant::now();
        collision_manager.clear();
        for (entity, physical) in (&*entities, &physicals).join() {
            let aabb = physical.world_aabb().expand_out(physical.vel);
            let shape = physical.shape.with_transform(physical.pos, physical.rot);
            collision_manager.add(aabb, CollisionMask::PHYSICAL, shape, entity);
        }
        for (entity, brick) in (&*entities, &bricks).join() {
            let brick_data = &bds[brick.data_id];
            let aabb = BrickAABB::new_orient(brick.pos, brick_data.size, brick.orientation);
            let world_aabb: WorldAABB = aabb.into();
            let brick_shape = Shape::Rectangle(world_aabb.radius).with_transform(world_aabb.center, Quaternion::one());
            collision_manager.add(world_aabb, CollisionMask::BRICK, brick_shape, entity);
        }
        struct C(Entity, Entity);
        impl Eq for C {}
        impl PartialEq<C> for C {
            fn eq(&self, other: &C) -> bool {
                let mut s0 = self.0;
                let mut s1 = self.1;
                let mut o0 = other.0;
                let mut o1 = other.1;
                if s0 > s1 {
                    let t = s0;
                    s0 = s1;
                    s1 = t;
                }
                if o0 > o1 {
                    let t = o0;
                    o0 = o1;
                    o1 = t;
                }
                (s0, s1) == (o0, o1)
            }
        }
        impl Hash for C {
            fn hash<H: Hasher>(&self, state: &mut H) {
                if self.0 > self.1 {
                    self.0.hash(state);
                    self.1.hash(state);
                } else {
                    self.1.hash(state);
                    self.0.hash(state);
                }
            }
        }
        let mut candidates: HashSet<C> = HashSet::new();
        for cell in collision_manager.cells() {
            for i in 0..cell.len() {
                let (entity1, _, aabb1, _) = cell[i];
                let is_brick = bricks.contains(entity1);
                for j in i + 1..cell.len() {
                    let (entity2, _, aabb2, _) = cell[j];
                    if is_brick && bricks.contains(entity2) {
                        continue;
                    }
                    if aabb1.intersects(aabb2) {
                        candidates.insert(C(entity1, entity2));
                    }
                }
            }
        }
        stats
            .broad_phase_collision_time
            .push(start_broad.elapsed().subsec_nanos() / 1000_000);
        stats.narrow_phase_collisions.push(candidates.len() as u32);
        let start_narrow = Instant::now();
        let mut collisions = Vec::with_capacity(candidates.len());
        for C(a, b) in candidates {
            if bricks.contains(a) && bricks.contains(b) {
                panic!("bricks should not be able to collide, something went really wrong");
            }
            let shape_a = if let Some(brick) = bricks.get(a) {
                let brick_data = &bds[brick.data_id];
                let brick_aabb =
                    BrickAABB::new_orient(brick.pos, brick_data.size, brick.orientation);
                let world_aabb = WorldAABB::from(brick_aabb);
                Shape::Rectangle(world_aabb.radius)
                    .with_transform(world_aabb.center, Quaternion::one())
            } else if let Some(physical) = physicals.get(a) {
                physical.transformed_shape()
            } else {
                unreachable!()
            };
            let shape_b = if let Some(brick) = bricks.get(b) {
                let brick_data = &bds[brick.data_id];
                let brick_aabb =
                    BrickAABB::new_orient(brick.pos, brick_data.size, brick.orientation);
                let world_aabb = WorldAABB::from(brick_aabb);
                Shape::Rectangle(world_aabb.radius)
                    .with_transform(world_aabb.center, Quaternion::one())
            } else if let Some(physical) = physicals.get(b) {
                physical.transformed_shape()
            } else {
                unreachable!()
            };
            if let Some(collision) = gjk_collide(&shape_a, &shape_b) {
                collisions.push(Collision {
                    entity_a: a,
                    entity_b: b,
                    normal: collision.normal,
                    depth: collision.depth,
                });
            }
        }
        stats
            .narrow_phase_collision_time
            .push(start_narrow.elapsed().subsec_nanos() / 1000_000);
        collision_manager.events.drain_vec_write(&mut collisions);
    }
}
