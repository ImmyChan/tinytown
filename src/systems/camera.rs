use {cgmath::Vector3, specs::prelude::*};

use crate::{
    camera::Camera,
    components::{Physical, Player},
};

pub struct CameraSystem;

impl<'a> System<'a> for CameraSystem {
    type SystemData = (
        ReadStorage<'a, Player>,
        ReadStorage<'a, Physical>,
        WriteExpect<'a, Camera>,
    );

    fn run(&mut self, (players, physicals, mut camera): Self::SystemData) {
        let player_eye_offset = Vector3::new(0., 1., 0.);
        for (player, physical) in (&players, &physicals).join() {
            camera.eye = physical.pos + player_eye_offset;
            camera.dir = player.facing;
        }
    }
}
