use {
    std::path::PathBuf,
    structopt::StructOpt,
};

mod backend;
mod backends;
mod brick;
mod camera;
mod collision;
mod components;
mod consts;
mod controls;
mod effect;
mod error;
mod game;
mod loaders;
mod math_ext;
mod mesh;
mod resources;
mod systems;
mod texture_manager;
mod util;

#[cfg(test)]
mod testutil;

use crate::{backend::Backend, game::Game};

const WINDOW_SIZE: (u32, u32) = (960, 960);
const WINDOW_TITLE: &'static str = "Tinytown";

#[derive(StructOpt)]
struct Opt {
    /// Load a Blockland save file.
    #[structopt(short = "l", long = "load-bls", parse(from_os_str))]
    save_file: Option<PathBuf>,

    /// List backends.
    #[structopt(short = "b", long = "list-backends")]
    list_backends: bool,

    /// Select backend.
    #[structopt(short = "s", long = "select-backend")]
    backend: Option<String>,
}

macro_rules! choose_backend {
    ($name:expr, $cont:expr, $($args:expr),*) => {
        match $name {
            #[cfg(feature = "backend_luminance")]
            "luminance" => {
                use backends::luminance::LuminanceBackend;
                $cont(LuminanceBackend::new(WINDOW_SIZE, WINDOW_TITLE)?, $($args),*)
            },
            #[cfg(feature = "backend_null")]
            "null" => {
                use backends::null::NullBackend;
                $cont(NullBackend::new(), $($args),*)
            },
            _ => panic!("invalid backend"),
        }
    }
}

fn main() -> anyhow::Result<()> {
    pretty_env_logger::init();
    let opt = Opt::from_args();
    let backends = backends::available_backends();
    if opt.list_backends {
        println!("Available backends:");
        for backend in &backends {
            println!(" - {}", backend);
        }
        return Ok(());
    }
    let selected_backend = if let Some(ref backend_name) = opt.backend {
        backends
            .iter()
            .find(|&&backend| backend == backend_name)
            .expect("invalid backend name")
    } else {
        &backends[0]
    };
    choose_backend!(*selected_backend, cont, opt)
}

fn cont<B: Backend>(backend: B, opt: Opt) -> anyhow::Result<()> {
    let mut game = Game::new(backend);
    game.init()?;
    if let Some(ref save_file) = opt.save_file {
        game.load_bls(save_file)?;
    }
    game.start()?;
    Ok(())
}
